<?php //funciones pequeñas
defined('BASEPATH') OR exit('No direct script access allowed');

class Help_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	//obtiene el almacen del usuario
	function getalmacen($alm){
		$sql="SELECT A.*
		from almacen A
		where A.id=$alm";
		$consulta = $this->db->query($sql);
		return $consulta->row_array();
	}
	//obtiene la sucursal del usuario
	function getsucursal($id){
		$sql="SELECT S.*
		from Sucursal S 
		where S.id=$id";
		$consulta = $this->db->query($sql);
		return $consulta->row_array();
	}
	//borra un elemento o todos segun el id
	public function delete($slug, $id){
		return $this->db->delete($slug ,array('id'=>$id));
	}
	//borra un elemento o todos segun el id y el atributo
	public function deletemore($slug, $value, $id){
		return $this->db->delete($slug ,array($value=>$id));
	}
	//para incertar una notificacion
	public function setnoti($usu, $id, $tipo, $cod, $alm, $men)
	{
		$info=array(
			'usuario_id'=>$usu,
			'nid'=>$id,
			'codigo'=>$cod,
			'tipo_id'=>$tipo,
			'almacen_id'=>$alm,
			'fecha'=>date("Y-m-d H:i:s"));
		$this->db->insert('notificacion',$info);
		$log=array(
			'usuario_id'=>$usu,
			'accion'=>$men.$id,
			'fecha'=>date("Y-m-d H:i:s"));
		return $this->db->insert('log',$log);
	}
	//busca si el nick no esta en uso en la columna de la tabla
	public function existe($nick, $codigo, $tabla){
		$sql="SELECT case when '$nick' in (SELECT $codigo from $tabla) then 1 else 0 end as exist";
		$consulta = $this->db->query($sql);
		return $consulta->row_array()['exist'];
	}
	//busca si el nick no esta en uso en la columna de la tabla y si esta activo
	public function existeand($nick, $codigo, $tabla){
		$sql="SELECT case when '$nick' in (SELECT $codigo from $tabla where status=1) then 1 else 0 end as exist";
		$consulta = $this->db->query($sql);
		return $consulta->row_array()['exist'];
	}
	//busca si la factura ya se ha emitido para ese proveedor
	public function facturado($id, $auto, $prov){
		$sql="SELECT case when '$id' in (select factura
			from nota_recepcion N
			where N.externo_id=$prov and N.autorizacion=$auto ) then 1 else 0 end as exist";
		$consulta = $this->db->query($sql);
		return $consulta->row_array()['exist'];
	}
	//para obtener un solo elemento de una tabla
	function getonly($value, $id){
		return  $this->db->get_where($value, array('id'=>$id))->row_array();
	}
	//para obtener un varios elementos de una tabla por un criterio
	function getmore($value, $id, $crit){
		return  $this->db->get_where($value, array($crit=>$id))->result_array();
	}
	//deja invisible un elemento o todos segun el id
	public function hidden($slug, $id){
		return $this->db->update($slug,array('status' => 1), array('id'=>$id));
	}
	//deja visible un elemento o todos segun el id
	public function visible($slug, $id){
		return $this->db->update($slug,array('status' => 0), array('id'=>$id));
	}
	//para colocar el resumen a un proyecto
	public function resume($id, $bit)
	{
		return $this->db->update('proyecto',array('resume'=> $bit ), array('id'=>$id));
	}
	//metodo temporal para asignarles un proveedor a los productos
	public function insertar()
	{
		for ($i=1; $i <1073 ; $i++) { 
			$this->db->insert('proveedor_producto',array('producto_id'=>$i, 'proveedor_id'=>4));
		}
		return;
	}
	//para quitar las comillas de los productos
	public function arreglar()
	{
		$cat = $this->db->get('producto')->result_array();
		foreach ($cat as $value) {
			//$this->db->update('producto',array('codigo'=>str_replace('"', '', $value['codigo']) ,'nombre'=>str_replace('"', 'plg', $value['nombre']) ), array('id'=>$value['id']));
			$this->db->update('producto',array('nombre'=>strtolower($value['nombre']) ), array('id'=>$value['id']));
		}
		return;
	}
	//cambia el estado de un elemento o todos segun el id
	public function estado($slug, $id, $est){
		return $this->db->update($slug,array('estado' => $est), array('id'=>$id));
	}
}

/* End of file cliente_model */
/* Location: ./application/models/cliente_model */
