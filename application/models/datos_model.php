<?php //funciones pequeñas
defined('BASEPATH') OR exit('No direct script access allowed');

class Datos_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	//obtiene todas las noticias
	function getall(){
		$sql="select CONCAT_WS('-',A.cod,N.codigo) as codigo, N.nombre, N.fecha_noticia as fecha, N.id 
		from noticias N, amenaza A
		where N.amenaza=A.id
		order by fecha desc";
		$consulta = $this->db->query($sql);
		return $consulta->result_array();
	}
	//obtiene una noticia por el id
	function getnoticia($id){
		$sql="select CONCAT_WS('-',A.cod,N.codigo) as codigo,A.nombre as amenaza, A.id as tipo,
				N.id, N.estado, N.fecha_notificacion, N.nombre, N.inicio, N.fin, N.ejecutado,
		(select TM.nombre from tmedio TM where TM.id=N.tipo_medio) as tmedio
		,(select M.nombre from medio M where M.id=N.medio) as medio,
		(select E.nombre from medida E where E.id=N.tipo_medida) as medida, N.nmedida, N.conflicto, N.imp,
		N.procedencia, N.valoracion, N.credibilidad, N.pertinencia, F.nombre as fuente, 
		N.tipo_medida, N.val_riesgo, N.lugar, N.coordenadas, N.enlace,N.imagen, 
		N.descripcion, N.fecha_noticia, N.resumen, N.probabilidad as prob
		from noticias N, amenaza A, fuente F
		where N.id=$id and N.amenaza=A.id and F.id=N.procedencia";
		$consulta = $this->db->query($sql);
		return $consulta->row_array();
	}
	//obtiene las subamenazas de una noticia por el id
	function gettipos($id){
		$sql="select TA.nombre as atipo, SA.nombre as asub
			from tamenaza TA right join subamenaza SA on SA.tipo=TA.id, ntipo T
			where $id=T.id_noticia and SA.id=T.id_tipo";
		$consulta = $this->db->query($sql);
		return $consulta->result_array();
	}
	//obtiene los impactos de una noticia por el id
	function getimpactos($id=false){
		if($id===false){
			$sql="select I.id, I.nombre, T.nombre as tipo, T.id as es
			from impactos I, timpacto T
			where T.id=I.tipo
			order by tipo desc";
		}else{
			$sql="select P.cantidad, I.nombre, T.nombre as tipo, P.detalle, T.id as es
			from noticias N, impactos I, produce_impactos P, timpacto T
			where N.id=$id and N.id=P.cod_noticia and P.cod_impacto=I.id and T.id=I.tipo
			order by tipo desc, I.id desc";
		}
		$consulta = $this->db->query($sql);
		return $consulta->result_array();
	}
	//obtiene las partes de una noticia por el id, 0=demandante, 1=demandada
	function getpartes($id, $tipo){
		$sql="select concat(P.articulo, P.nombre) as nombre
		from noticias N, partes P, tiene_partes T
		where N.id=$id and N.id=T.cod_noticia and T.cod_parte=P.cod_parte and T.parte=$tipo and P.tipo=1
		union
		select CONCAT(P.articulo, P.nombre,' (', T.detalle, ')') as nombre
				from noticias N, partes P, tiene_partes T
				where N.id=$id and N.id=T.cod_noticia and T.cod_parte=P.cod_parte and T.parte=$tipo and P.tipo=2
		union
		select CONCAT(P.articulo, P.nombre,' de ', D.nombre) as nombre
				from noticias N, partes P, tiene_partes T, departamento D
				where N.id=$id and N.id=T.cod_noticia and T.cod_parte=P.cod_parte and T.parte=$tipo and P.tipo=3 and D.id=T.detalle
		union
		select CONCAT(P.articulo, P.nombre,' (', S.nombre, ')') as nombre
				from noticias N, partes P, tiene_partes T, sector S
				where N.id=$id and N.id=T.cod_noticia and T.cod_parte=P.cod_parte and T.parte=$tipo and P.tipo=4 and S.id=T.detalle
		";
		$consulta = $this->db->query($sql);
		return $consulta->result_array();
	}
	//para insertar una noticia
	public function setnoticia($usu)
	{
		$log=array(
			'nombre'=>$this->input->post('nombre'),
			'amenaza'=>$this->input->post('amenaza'),
			// 'tipo_amenaza'=>$samenaza,
			'medio'=>$this->input->post('medio'),
			'codigo'=>explode('-', $this->input->post('codigo'))[1],
			'estado'=>$this->input->post('estado'),
			'tipo_medio'=>$this->input->post('tmedio'),
			'procedencia'=>$this->input->post('fuentep'),
			'valoracion'=>$this->input->post('fuentev'),
			'credibilidad'=>$this->input->post('infoc'),
			'pertinencia'=>'1-'.$this->input->post('infop1').';2-'.$this->input->post('infop2').';3-'.$this->input->post('infop3').';4-'.$this->input->post('infop4'),
			'tipo_medida'=>$this->input->post('medida'),
			'nmedida'=>$this->input->post('emedida'),
			'val_riesgo'=>$this->input->post('riesgo'),
			'fecha_noticia'=>$this->input->post('nfecha'),
			'lugar'=>$this->input->post('lugar'),
			'coordenadas'=>$this->input->post('coordenadas'),
			'descripcion'=>$this->input->post('hecho'),
			'enlace'=>$this->input->post('link'),
			'probabilidad'=>$this->input->post('prob'),
			'imagen'=>$this->input->post('img'),
			'inicio'=>$this->input->post('inicio'),
			'fin'=>$this->input->post('fin'),
			'ejecutado'=>$this->input->post('ejecutor'),
			'usuario'=>$usu,
			'fecha_notificacion'=>$this->input->post('fecha'),
			'conflicto'=>$this->input->post('ambito'),
			'imp'=>$this->input->post('imp')
		);
		$this->db->insert('noticias',$log);
		$sql="select max(N.id) as id
		from noticias N";
		$consulta = $this->db->query($sql);
		$id=$consulta->row_array()['id'];
		//subamenazas
		if($this->input->post('subs')!=array()){
			$subs=$this->input->post('subs');
			foreach ($subs as $value) {
				$parte=array(
					'id_noticia'=>$id,
					'id_tipo'=>$value
				);
				$this->db->insert('ntipo',$parte);
			}
		}
		if($this->input->post('act1')!=array()){
			$prot=$this->input->post('act1');
			foreach ($prot as $value) {
				/*if($value==5){
					$detalle="";
					$secs=$this->input->post('parte_5_1');
					$hola="";
					foreach ($secs as $key => $value) {
						$detalle.=$hola.$key.'-'.$value;
						$hola=";";
					}
				}
				else*/
					$detalle=$this->input->post('parte_'.$value.'_1');
				$parte=array(
					'cod_noticia'=>$id,
					'cod_parte'=>$value,
					'parte'=>0,
					'detalle'=>$detalle
				);
				$this->db->insert('tiene_partes',$parte);
			}
		}
		if($this->input->post('act0')!=array()){
			$anta=$this->input->post('act0');
			foreach ($anta as $value) {
				$detalle=$this->input->post('parte_'.$value.'_0');
				$parte=array(
					'cod_noticia'=>$id,
					'cod_parte'=>$value,
					'parte'=>1,
					'detalle'=>$detalle
				);
				$this->db->insert('tiene_partes',$parte);
			}
		}
		if($this->input->post('check[]')!=array()){
			$check=$this->input->post('check[]');
			foreach ($check as $value) {
				$num=$this->input->post('imp_'.$value);
				$detalle=$this->input->post('det_'.$value);
				$parte=array(
					'cod_noticia'=>$id,
					'cod_impacto'=>$value,
					'cantidad'=>$num,
					'detalle'=>$detalle
				);
				$this->db->insert('produce_impactos',$parte);
			}
		}
		return $id;
	}
	//para obtener un solo elemento de una tabla
	function getonly($value, $id){
		return  $this->db->get_where($value, array('id'=>$id))->row_array();
	}
	//para obtener todos lo elementos de una tabla
	function gettabla($value){
		return $this->db->get($value)->result_array();
	}
	//para obtener un varios elementos de una tabla por un criterio
	function getmore($value, $id, $crit){
		return  $this->db->get_where($value, array($crit=>$id))->result_array();
	}
	//obtiene todas las amenazas
	function amenazas(){
		$sql="select A.*, ifnull((select max(N.codigo)
		from noticias N
		where A.id=N.amenaza), 0)+1 as last
		from amenaza A
		order by tipo, nombre";
		$consulta = $this->db->query($sql);
		return $consulta->result_array();
	}
	//obtiene todas las subamenazas
	function samenazas(){
		
		$am=$this->db->get('amenaza')->result_array();
		$sa=array();
		foreach ($am as $value) {
			$sql="select S.id,S.nombre, T.nombre as tipo
			from subamenaza S left join tamenaza T
			on S.tipo=T.id
			where S.amenaza=".$value['id']."
			ORDER BY tipo, nombre";
			$consulta = $this->db->query($sql);
			$ms[$value['id']]=$consulta->result_array();
		}
		return $ms;
		
	}
	//obtiene todos los medios
	function getmedios(){
		$tm=$this->db->get('tmedio')->result_array();
		$ms=array();
		foreach ($tm as $value) {
			$sql="SELECT M.id, M.nombre, M.tipo, D.nombre as lugar
			from medio M LEFT JOIN departamento D on M.lugar=D.id
			where M.tipo=".$value['id']." order by D.id";
			$consulta = $this->db->query($sql);
			$ms[$value['id']]=$consulta->result_array();
		}
		return $ms;
	}
	//borra un elemento o todos segun el id
	public function setresumen($id){
		return $this->db->update('noticias',array('resumen'=>$this->input->post('resumen')), array('id'=>$id));
	}
	//obtiene la información para mostrar en el json
	function getjson($tipo=false){
		if($tipo===false){
			$sql="select N.nombre, N.id, I.link as icon, N.coordenadas
			from noticias N, icono I
			order by N.fecha_notificacion desc";
		}else{
			$sql="select N.nombre, N.id, I.link as icon, N.coordenadas
			from $tipo N, icono I
			order by N.nombre";
		}
		$consulta = $this->db->query($sql);
		return $consulta->result_array();
	}
	//agregar un nuevo elemento
	public function setcampo($tipo)
	{
		$datos=array();
		foreach ($_POST as $key => $value) {
			$datos[$key]=$value;
		}
		return $this->db->insert($tipo,$datos);
	}
	//editar un elemento
	public function editcampo($id, $tipo)
	{
		$datos=array();
		foreach ($_POST as $key => $value) {
			$datos[$key]=$value;
		}
		return $this->db->update($tipo, $datos, array('id'=>$id));
	}
	//eliminar un elemento
	public function dropcampo($id, $tipo)
	{
		return $this->db->delete($tipo, array('id'=>$id));
	}
	//
	//obtiene la información para mostrar en el mapa
	function getdmapa($id){
		$sql="select N.imagen, N.resumen
		from noticias N
		where N.id=$id";
		$consulta = $this->db->query($sql);
		return $consulta->row_array();
	}
	/*
	
	
	//borra un elemento o todos segun el id y el atributo
	public function deletemore($slug, $value, $id){
		return $this->db->delete($slug ,array($value=>$id));
	}
	//para incertar una notificacion
	public function iniciar($tt)
	{
		$log=array(
			'persona_id'=>$this->input->get('per'),
			'externo_id'=>$this->input->get('pro'),
			'tipo'=>$tt,
			'hora_inicial'=>$this->input->get('hora'),
			'fecha'=>date("Y-m-d")
		);
		$this->db->insert('control_personal',$log);
		return true;
	}
	//para incertar una notificacion
	public function finalizar($tt)
	{
		$log=array(
			'hora_final'=>$this->input->get('hora')
		);
		$data=array(
			'persona_id'=>$this->input->get('per'),
			'externo_id'=>$this->input->get('pro'),
			'tipo'=>$tt,
			'fecha'=>date("Y-m-d")
		);
		$this->db->update('control_personal',$log, $data);
		return true;
	}
	public function obs($tt)
	{
		$log=array(
			'obs'=>$this->input->get('obs')
		);
		$data=array(
			'persona_id'=>$this->input->get('per'),
			'externo_id'=>$this->input->get('pro'),
			'tipo'=>$tt,
			'fecha'=>date("Y-m-d")
		);
		$this->db->update('control_personal',$log, $data);
		return $this->input->get('obs');
	}

	//busca si el nick no esta en uso en la columna de la tabla
	public function existe($nick, $codigo, $tabla){
		$sql="SELECT case when '$nick' in (SELECT $codigo from $tabla) then 1 else 0 end as exist";
		$consulta = $this->db->query($sql);
		return $consulta->row_array()['exist'];
	}
	
	//deja invisible un elemento o todos segun el id
	public function hidden($slug, $id){
		return $this->db->update($slug,array('status' => 1), array('id'=>$id));
	}
	public function getmes($mes)
	{
		$meses=array(
			'01'=>'Enero',
			'02'=>'Febrero',
			'03'=>'Marzo',
			'04'=>'Abril',
			'05'=>'Mayo',
			'06'=>'Junio',
			'07'=>'Julio',
			'08'=>'Agosto',
			'09'=>'Septiembre',
			'10'=>'Octubre',
			'11'=>'Noviembre',
			'12'=>'Diciembre'
		);
		return $meses[$mes];
	}
	//obtiene todo el personal de un soporte para el control diariio
	function getsoporte($id){
		$sql="SELECT p.id, CONCAT_WS(' ',p.nombre, p.paterno, p.materno) as nombre, cp.hora_inicial, cp.hora_final
		FROM (tecnico AS t INNER JOIN persona AS p ON t.tecnico_id=p.id)
		INNER JOIN control_personal AS cp ON cp.persona_id=p.id and cp.fecha='".date('Y/m/d')."' and cp.externo_id = $id and cp.tipo=1";
		$consulta = $this->db->query($sql);
		return $consulta->result_array();
	}
	//proyectos segun encargado
	public function proyectos($id)
	{
		$sql="SELECT pr.id AS proyecto_id, CONCAT_WS('-', c.codigo, pr.codigo) AS codigo, pr.des, pr.status,pr.ingreso,pr.salida,
			    c.nombre, IF(pr.status=0,'Vigente',IF(pr.status=1,'Finalizado','Cierre Parcial')) AS estado
			FROM proyecto AS pr INNER JOIN cliente AS c
			ON pr.encargado=$id AND pr.cliente_id=c.id
			ORDER BY proyecto_id DESC";
		$consulta = $this->db->query($sql);
		return $consulta->result_array();
	}
	//soportes segun encargado
	public function soportes($id)
	{
		$am=$this->db->get('amenaza')->result_array();
		foreach ($am as $value) {
			$sa.='{"id":'.$value['id'].',';
			$sa.='"nombre":"'.$value['nombre'].'","subs":[';
			$sql="select S.id,S.nombre, T.nombre as tipo
			from subamenaza S left join tamenaza T
			on S.tipo=T.id
			where S.amenaza=".$value['id']."
			ORDER BY tipo, nombre";
			$consulta = $this->db->query($sql);
			$ms=$consulta->result_array();
			foreach ($ms as $key) {
				$sa.='{"id":'.$key['id'].',';
				$sa.='"nombre":"'.$key['nombre'].'"},';
			}
			$sa.=']}';
		}
		return $sa.']';
	}*/
}

/* End of file cliente_model */
/* Location: ./application/models/cliente_model */
