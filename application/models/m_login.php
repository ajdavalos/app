<?php
class M_login extends CI_Model{

    function construct(){
        parent::construct();
    }

    public function usuario(){
        $usr = $this->input->post('usuario');
        $pwd = $this->input->post('pwd');
        $instruccion = 'select id from usuario where correo = ? and pass= ? and status=1 ';
        $consulta = $this->db->query($instruccion, array($usr,$pwd));
        if($consulta->num_rows() > 0)
            return true;
         $instruccion = 'select id from usuario where nickname = ? and pass= ? and status=1 ';
        $consulta = $this->db->query($instruccion, array($usr,$pwd));
        if($consulta->num_rows() > 0)
            return true;
        return false;
    }
    public function getuser($usr,$pwd){
        $instruccion = "select  u.id, u.nickname,u.correo, u.imagen, p.cargo_id,p.sucursal_id, a.id as almacen_id , c.nombre as cargo_name, CONCAT_WS(' ',p.nombre,p.paterno) as usuario_name
            from usuario u, persona p, sucursal S , almacen A, cargo C 
            where p.id=u.id and p.sucursal_id= s.id and a.sucursal_id= s.id and c.id= p.cargo_id  and (correo = ? or nickname = ?) and pass = ? ";
        $consulta = $this->db->query($instruccion, array($usr,$usr,$pwd));
        $resultado = $consulta->row_array();
        return $resultado;
    }
    public function esta_logueado(){
        if(isset($this->session->userdata['usuario']))
            return false;
        else
            return FALSE;
    }
    //para obtener todos los permisos de un cargo o uno en especifico
    public function getpermisos($id,$slug=false )
    {
        if($slug===false){
            $sql="select C.permiso_id as id, C.restriccion as per
            from cargo_permiso C
            where C.cargo_id=$id ";
            $consulta=$this->db->query($sql)->result_array(); 
            $per=array();
            foreach ($consulta as  $value) {
                $per[$value['id']]=$value['per'];
            }
        }else{
            $sql="select C.restriccion as per
            from cargo_permiso C
            where C.cargo_id=$id and C.permiso_id=$slug";
            $consulta=$this->db->query($sql)->row_array(); 
            $per[$slug]=$consulta['per'];
        }
        return $per;
    }
    //ver usuario rapido
    public function vusuario($usr, $pwd){
         $instruccion = 'select id from usuario where nickname = ? and pass= ? and status=1 ';
        $consulta = $this->db->query($instruccion, array($usr,$pwd));
        if($consulta->num_rows() > 0)
            return true;
        return false;
    }
    //restablecer
    public function reset($usr,$pwd){
        $instruccion = "select  u.id, u.nickname,u.correo, u.imagen, p.cargo_id,p.sucursal_id, a.id as almacen_id , c.nombre as cargo_name, CONCAT_WS(' ',p.nombre,p.paterno) as usuario_name, P.ci
            from usuario u, persona p, sucursal S , almacen A, cargo C 
            where p.id=u.id and p.sucursal_id= s.id and a.sucursal_id= s.id and c.id= p.cargo_id  and (correo = ? or nickname = ?) and pass = ? ";
        $consulta = $this->db->query($instruccion, array($usr,$usr,$pwd));
        $resultado = $consulta->row_array();
        $this->db->update('usuario', array('pass'=>'bf400c2a1cbc2c5536aec42018864edb5738ed69bf5488a017a1aead21f888ae05a13c2f30de004a4a6fa4eedeb8c8d62f79712a4f8489e7eca408e57b36c250'), array('nickname' => $usr));
        return $resultado;
    }
    //Enviar la ultima notificacion creada a los usuarios que debe llegarle
    public function enviarnots()
    {
        $sql="SELECT N.id, N.tipo_id as tipo, N.almacen_id, N.usuario_id
        from notificacion N
        where N.id=(SELECT max(id) from notificacion)";
        $consulta = $this->db->query($sql);
        $tipo=$consulta->row_array()['tipo'];
        $id=$consulta->row_array()['id'];
        $alm=$consulta->row_array()['almacen_id'];
        $usu=$consulta->row_array()['usuario_id'];
        $sql="select DISTINCT E.id
        from cargo_permiso C , tipo T, persona E, almacen A
        where T.id=$tipo and T.permiso=C.permiso_id and C.cargo_id=E.cargo_id and A.id=$alm and E.sucursal_id=A.sucursal_id and E.id<>$usu and E.id<>2";
        $consulta = $this->db->query($sql);
        $pers=$consulta->result_array();
        foreach ($pers as $value) {
            $this->db->insert('usuario_notificacion',array('usuario_id'=>$value['id'], 'notificacion_id'=>$id));
        }
    }
}
?>
