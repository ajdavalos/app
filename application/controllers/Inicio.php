<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	//index
	public function index()
	{
		$this->load->view('head');
		$this->load->view('index');
		$this->load->view('foot');
	}
	//nueva informacion
	public function nuevo()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nombre','Nombre','required');
		if($this->form_validation->run() === false ){
			$this->load->view('head');
			$this->load->model('datos_model');
			$data['medida']=$this->datos_model->gettabla('medida');
			$data['sector']=$this->datos_model->gettabla('sector');
			$data['depr']=$this->datos_model->gettabla('departamento');
			$data['tmedio']=$this->datos_model->gettabla('tmedio');
			$data['fuente']=$this->datos_model->gettabla('fuente');
			$data['amenaza']=$this->datos_model->amenazas();
			$data['samenaza']=$this->datos_model->samenazas();
			$data['medio']=$this->datos_model->getmedios();
			$data['imp']=$this->datos_model->getimpactos();//impactos
			$this->load->view('nuevo', $data);
			$this->load->view('foot');
		}else{
			$archivo='img';
			$config['upload_path']="img/noticias/";
			// $config['file_name']=$id;
			$config['allowed_types']="png|gif|jpg";					
			$config['max_size']="5000";
			$config['overwrite']=false;
			$config['max_width']="5000";
			$config['max_height']="5000";
			$file=true;
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload($archivo)){
				//ocurrio un error
				$data['uploadError']=$this->upload->display_errors();
				if(!strpos($data['uploadError'], "not select a file to upload")){
					$data['dato']="<script language='Javascript'>  alert('La imagen no se ha subido correctamente, seleccione otra imagen.'); </script>";
					$this->load->view('cero', $data);//OBLIGATORIO
				}
				$file=null;
			}
			$img=$this->upload->data();
			if($file){ 
				$_POST['img']=$img['file_name']; 
			}else{
				$_POST['img']='0.png'; 
			}
			$this->load->model('datos_model');
			$id=$this->datos_model->setnoticia(1);
			redirect('inicio/ver/'.$id);
			// $this->load->view('cero');
		}
	}
	//ver informacion
	public function ver($id)
	{
		$this->load->view('head');
		$this->load->model('datos_model');
		$data['datos']=$this->datos_model->getnoticia($id);
		$data['imp']=$this->datos_model->getimpactos($id);//impactos
		$data['sub']=$this->datos_model->gettipos($id);//amenazas
		if($data['datos']['tipo']==4){
			$data['sector']=$this->datos_model->gettabla('sector');
			$data['prota']=$this->datos_model->getpartes($id, 0);
			$data['anta']=$this->datos_model->getpartes($id, 1);
		}
		$this->load->view('ver', $data);
		if($data['datos']['resumen']==''){
			// echo $_POST['resumen'];
			$this->datos_model->setresumen($id);
		}
		$this->load->view('foot');
	}
	//ver informacion
	public function detalles($id)
	{
		$this->load->view('head');
		$this->load->model('datos_model');
		$data['datos']=$this->datos_model->getnoticia($id);
		$data['imp']=$this->datos_model->getimpactos($id);//impactos
		$data['sub']=$this->datos_model->gettipos($id);//amenazas
		if($data['datos']['tipo']==4){
			$data['sector']=$this->datos_model->gettabla('sector');
			$data['prota']=$this->datos_model->getpartes($id, 0);
			$data['anta']=$this->datos_model->getpartes($id, 1);
		}
		$this->load->view('detalles', $data);
		// $this->load->view('cero');
		$this->load->view('foot');
	}
	public function geocodificacion()
	{
		$this->load->view('head');
		$this->load->view('geocodificacion.html');
		$this->load->view('foot');
	}
	//ver todas las informaciones
	public function todas()
	{
		$this->load->view('head');
		$this->load->model('datos_model');
		$data['notis']=$this->datos_model->getall();
		$this->load->view('todas', $data);
		$this->load->view('foot');
	}
	//editar los campos del formulario
	public function formulario()
	{
		$this->load->view('head');
		$this->load->model('datos_model');
		$data['medida']=$this->datos_model->gettabla('medida');
		$data['sector']=$this->datos_model->gettabla('sector');
		$data['depr']=$this->datos_model->gettabla('departamento');
		$data['tmedio']=$this->datos_model->gettabla('tmedio');
		$data['tamz']=$this->datos_model->gettabla('tamenaza');
		$data['fuente']=$this->datos_model->gettabla('fuente');
		$data['amenaza']=$this->datos_model->amenazas();
		$data['samenaza']=$this->datos_model->samenazas();
		$data['medio']=$this->datos_model->getmedios();
		$data['imp']=$this->datos_model->getimpactos();//impactos
		$this->load->view('formulario', $data);
		$this->load->view('foot');
	}
	//agregar un nuevo elemento
	public function agregar($tipo, $ext=false)
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nombre','Nombre','required');
		if($this->form_validation->run() === false ){
			$this->load->view('head');
			$data['tipo']=$tipo;
			$data['ext']=$ext;
			if($tipo=='medio'){
				$this->load->model('datos_model');
				$data['depr']=$this->datos_model->gettabla('departamento');
				$data['tmedio']=$this->datos_model->gettabla('tmedio');
			}
			if($tipo=='subamenaza'){
				$this->load->model('datos_model');
				$data['amz']=$this->datos_model->amenazas();
				$data['tamz']=$this->datos_model->gettabla('tamenaza');
			}
			if($tipo=='impactos'){
				$this->load->model('datos_model');
				$data['timp']=$this->datos_model->gettabla('timpacto');
			}
			$this->load->view('agregar', $data);
			$this->load->view('foot');
		}else{
			$this->load->model('datos_model');
			$this->datos_model->setcampo($tipo, $ext=false);
			$this->load->view('bye');
		}
	}
	//editar un elemento
	public function editar($tipo, $id)
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nombre','Nombre','required');
		if($this->form_validation->run() === false ){
			$this->load->model('datos_model');
			$this->load->view('head');
			$data['tipo']=$tipo;
			// $data['ext']=$ext;
			$data['edit']=$this->datos_model->getonly($tipo, $id);
			if($tipo=='medio'){
				$data['depr']=$this->datos_model->gettabla('departamento');
				$data['tmedio']=$this->datos_model->gettabla('tmedio');
			}
			if($tipo=='subamenaza'){				
				$data['amz']=$this->datos_model->amenazas();
				$data['tamz']=$this->datos_model->gettabla('tamenaza');
			}
			if($tipo=='impactos'){
				$data['timp']=$this->datos_model->gettabla('timpacto');
			}
			$this->load->view('editar', $data);
			$this->load->view('foot');
		}else{
			$this->load->model('datos_model');
			$this->datos_model->editcampo($id, $tipo);
			$this->load->view('bye');
		}
	}
	//eliminar un elemento
	public function eliminar($tipo, $id)
	{
		$this->load->model('datos_model');
		$this->datos_model->dropcampo($id, $tipo);
	}
	//ver denuncias pendientes
	public function denuncias()
	{
		$this->load->view('head');
		$this->load->view('denuncias');
		$this->load->view('foot');
	}
	//ver una denincia
	public function denuncia($id)
	{
		$this->load->view('head');
		$this->load->view('denuncia');
		// $this->load->view('cero');
		$this->load->view('foot');
	}
	public function mapa()
	{
		$this->load->view('head');
		$this->load->view('mapa');	
		$this->load->view('foot');
	}
	//para recibir la ubicacion
	public function enviar()
	{
		$this->load->view('bye');
	}
	
	public function prueba()
	{
		$this->load->view('head');
		$this->load->view('prueba2');
		$this->load->view('foot');
	}
	//obtener los datos a travez de json para mostrar en el mapa
	public function getinfojson()
	{
		$this->load->model('datos_model');
		$data['json']=$this->datos_model->getjson();
		$this->load->view('json', $data);
	}
	//obtener los datos de las tablas a travez de json para mostrar en el mapa 
	public function getjson($tipo)
	{
		$this->load->model('datos_model');
		$data['json']=$this->datos_model->getjson($tipo);
		$this->load->view('json', $data);
	}
	//obtener los datos simple
	public function getnoticias()
	{
		$this->load->model('datos_model');
		$data['json']=$this->datos_model->getjson();
		$this->load->view('noticias', $data);
	}
	public function stack()
	{
		$this->load->model('datos_model');
		$data['obj']=$this->datos_model->samenazas();
		$this->load->view('cero', $data);
	}
	//extras
	public function direccion()
	{
		$this->load->view('head');
		$this->load->view('direccion');
		$this->load->view('foot');
	}
	public function ubicacion()
	{
		$this->load->view('head');
		$this->load->view('direccion.html');
		$this->load->view('foot');
	}
	//detalles de una noticia en el mapa
	public function dmapa($id)
	{
		$this->load->model('datos_model');
		$data['datos']=$this->datos_model->getdmapa($id);
		$this->load->view('dmapa', $data);
	}
}
