{
  "type": "FeatureCollection",
  "features": [
<?php $con='';
foreach ($json as $value) { 
   echo $con; $con="," ?>
    {
      "type": "Feature",
      "properties": {
        "marker-size": "medium",
        "icon": "<?php echo $value['icon'] ?>",
        "title":"<?php echo $value['nombre'] ?>",
        "description": "<?php echo base_url('inicio/ver/'.$value['id']) ?>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          <?php echo implode(',', array_reverse(explode(',', $value['coordenadas']))) ?>
        ]
      }
    }<?php } ?>
  ]
}
