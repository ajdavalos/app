<body>
	<div class="container">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<br>
					<div class="row">
						<div class="col-md-10">
						    <h4>Parametros del formulario</h4>
					    </div>
					    <div class="col-md-2 right">
					    	<a href="<?php echo base_url() ?>">Inicio</a>
					    	<a href="">Actualizar</a>
					    </div>
				    </div><br>
				    <?php $deps=array();
							foreach ($depr as $value) { 
								$deps[$value['id']]=$value['nombre'];
							} ?> 
				    <div class="row">
						<div class="col-md-6 center">
							<div class="row"><br>
								<div class="col-md-9">
								    <h5>Tipos de Medio</h5>
							    </div>
							    <div class="col-md-3">
							    	<button class="btn btn-primary right btn-sm" title="Nuevo Medio" onclick="abrirVentana('<?php echo base_url('inicio/agregar/tmedio/') ?>')" ><b>+ Nuevo</b></button>
							    </div>
						    </div>
							<table class="table-bordered table-hover table-striped" style="width: 100%">
								<tr>
									<th>N°</th>
									<th>Nombre</th>
									<th>Opciones</th>
								</tr>
								<?php $i=0;
								foreach ($tmedio as $value) { ?>
								<tr>
									<td><?= ++$i ?></td>
									<td><?php echo $value['nombre'] ?></td>
									<td>
										<button onclick="abrirVentana('<?php echo base_url('inicio/editar/tmedio/'.$value['id']) ?>')" class="btn btn-primary btn-sm">Editar</button> 
										<button onclick="borrar('tmedio', <?= $value['id'] ?>)" class="btn btn-danger btn-sm">Borrar</button>
									</td>
								</tr>
								<?php } ?>
							</table> <br>
						</div>

						<div class="col-md-6 center" style="">
							<div class="row"><br>
								<div class="col-md-9">
								    <h5>Fuentes</h5>
							    </div>
							    <div class="col-md-3">
							    	<button class="btn btn-primary right btn-sm" title="Nueva Fuente" onclick="abrirVentana('<?php echo base_url('inicio/agregar/fuente/') ?>')"><b>+ Nuevo</b></button>
							    </div>
						    </div>
							<table class="table-bordered table-hover table-striped" style="width: 100%">
								<tr>
									<th>N°</th>
									<th>Nombre</th>
									<th>Opciones</th>
								</tr>
								<?php $i=0; 
								foreach ($fuente as $value) { ?>
								<tr>
									<td><?= ++$i ?></td>
									<td><?php echo $value['nombre'] ?></td>
									<td>
										<button onclick="abrirVentana('<?php echo base_url('inicio/editar/fuente/'.$value['id']) ?>')" class="btn btn-primary btn-sm">Editar</button>
										<button onclick="borrar('fuente', <?= $value['id'] ?>)" class="btn btn-danger btn-sm">Borrar</button>
								</tr>
								<?php } ?>
							</table> <br>
						</div>
						<?php foreach ($tmedio as $value) { ?>
						<div class="col-md-6 center" style="">
							<div class="row"><br>
								<div class="col-md-9">
								    <h5>Medios de <?php echo $value['nombre'] ?></h5>
							    </div>
							    <div class="col-md-3">
							    	<button class="btn btn-primary right btn-sm" title="Nuevo Medio de <?php echo $value['nombre'] ?>" onclick="abrirVentana('<?php echo base_url('inicio/agregar/medio/'.$value['id']) ?>')" ><b>+ Nuevo</b></button>
							    </div>
						    </div>
							<table class="table-bordered table-hover table-striped" style="width: 100%">
								<tr>
									<th>N°</th>
									<th>Nombre</th>
									<th>Opciones</th>
								</tr>
								<?php $i=0;
								foreach ($medio[$value['id']] as $key) { ?>
								<tr>
									<td><?= ++$i ?></td>
									<td><?php echo $key['nombre']; if($key['lugar']!='') echo ' ('.$key['lugar'].')' ?></td>
									<td>
										<button onclick="abrirVentana('<?php echo base_url('inicio/editar/medio/'.$key['id']) ?>')" class="btn btn-primary btn-sm">Editar</button>
										<button onclick="borrar('medio', <?= $key['id'] ?>)" class="btn btn-danger btn-sm">Borrar</button>
								</tr>
								<?php } ?>
							</table> <br>
						</div>
						<?php } ?>
						<div class="col-md-6 center" style="">
							<div class="row"><br>
								<div class="col-md-9">
								    <h5>Sectores</h5>
							    </div>
							    <div class="col-md-3">
							    	<button class="btn btn-primary right btn-sm" title="Nuevo Sector" onclick="abrirVentana('<?php echo base_url('inicio/agregar/sector/') ?>')"><b>+ Nuevo</b></button>
							    </div>
						    </div>
							<table class="table-bordered table-hover table-striped" style="width: 100%">
								<tr>
									<th>N°</th>
									<th>Nombre</th>
									<th>Opciones</th>
								</tr>
								<?php $i=0; $deps=array();
								foreach ($sector as $value) { ?>
								<tr>
									<td><?= ++$i ?></td>
									<td><?php echo $value['nombre'] ?></td>
									<td>
										<button onclick="abrirVentana('<?php echo base_url('inicio/editar/sector/'.$value['id']) ?>')" class="btn btn-primary btn-sm">Editar</button>
										<button onclick="borrar('sector', <?= $value['id'] ?>)" class="btn btn-danger btn-sm">Borrar</button>
								</tr>
								<?php } ?>
							</table> <br>
						</div>
						<div class="col-md-6 center" style="">
							<div class="row"><br>
								<div class="col-md-9">
								    <h5>Medidas</h5>
							    </div>
							    <div class="col-md-3">
							    	<button class="btn btn-primary right btn-sm" title="Nueva Medida" onclick="abrirVentana('<?php echo base_url('inicio/agregar/medida/') ?>')" ><b>+ Nuevo</b></button>
							    </div>
						    </div>
							<table class="table-bordered table-hover table-striped" style="width: 100%">
								<tr>
									<th>N°</th>
									<th>Nombre</th>
									<th>Opciones</th>
								</tr>
								<?php $i=0; $deps=array();
								foreach ($medida as $value) { ?>
								<tr>
									<td><?= ++$i ?></td>
									<td><?php echo $value['nombre'] ?></td>
									<td>
										<button onclick="abrirVentana('<?php echo base_url('inicio/editar/medida/'.$value['id']) ?>')" class="btn btn-primary btn-sm">Editar</button>
										<button onclick="borrar('medida', <?= $value['id'] ?>)" class="btn btn-danger btn-sm">Borrar</button>
								</tr>
								<?php } ?>
							</table> <br>
						</div>
						<div class="col-md-12 center" style="">
							<div class="row"><br>
								<div class="col-md-9">
								    <h5>Impactos</h5>
							    </div>
							    <div class="col-md-3">
							    	<button class="btn btn-primary right btn-sm" title="Nuevo Impacto" onclick="abrirVentana('<?php echo base_url('inicio/agregar/impactos/') ?>')"><b>+ Nuevo</b></button>
							    </div>
						    </div>
							<table class="table-bordered table-hover table-striped" style="width: 100%">
								<tr>
									<th>N°</th>
									<th>Nombre</th>
									<th>Tipo</th>
									<th>Opciones</th>
								</tr>
								<?php $i=0; $deps=array();
								foreach ($imp as $value) { ?>
								<tr>
									<td><?= ++$i ?></td>
									<td><?= ucwords($value['nombre']) ?></td>
									<td><?= $value['tipo'] ?></td>
									<td>
										<button onclick="abrirVentana('<?php echo base_url('inicio/editar/impactos/'.$value['id']) ?>')" class="btn btn-primary btn-sm">Editar</button> 
										<button onclick="borrar('impactos', <?= $value['id'] ?>)" class="btn btn-danger btn-sm">Borrar</button>
								</tr>
								<?php } ?>
							</table> <br>
						</div>
						<div class="col-md-6 center" style="">
							<div class="row"><br>
								<div class="col-md-9">
								    <h5>Tipos de Amenaza</h5>
							    </div>
							    <div class="col-md-3">
							    	<button class="btn btn-primary right btn-sm" title="Nuevo Tipo de Amenaza" onclick="abrirVentana('<?php echo base_url('inicio/agregar/amenaza/') ?>')"><b>+ Nuevo</b></button>
							    </div>
						    </div>
							<table class="table-bordered table-hover table-striped" style="width: 100%">
								<tr>
									<th>N°</th>
									<th>Nombre</th>
									<th>Opciones</th>
								</tr>
								<?php $i=0; $deps=array();
								foreach ($amenaza as $value) { ?>
								<tr>
									<td><?= ++$i ?></td>
									<td><?php echo $value['nombre'] ?></td>
									<td>
										<button onclick="abrirVentana('<?php echo base_url('inicio/editar/amenaza/'.$value['id']) ?>')" class="btn btn-primary btn-sm">Editar</button>
										<button onclick="borrar('amenaza', <?= $value['id'] ?>)" class="btn btn-danger btn-sm">Borrar</button>
								</tr>
								<?php } ?>
							</table> <br>
						</div>
						<div class="col-md-6 center" style="">
							<div class="row"><br>
								<div class="col-md-9">
								    <h5>Subtipos de Amenaza</h5>
							    </div>
							    <div class="col-md-3">
							    	<button class="btn btn-primary right btn-sm" title="Nuevo Subtipo de Amenaza" onclick="abrirVentana('<?php echo base_url('inicio/agregar/tamenaza/') ?>')"><b>+ Nuevo</b></button>
							    </div>
						    </div>
							<table class="table-bordered table-hover table-striped" style="width: 100%">
								<tr>
									<th>N°</th>
									<th>Nombre</th>
									<th>Opciones</th>
								</tr>
								<?php $i=0; $deps=array();
								foreach ($tamz as $value) { ?>
								<tr>
									<td><?= ++$i ?></td>
									<td><?php echo $value['nombre'] ?></td>
									<td>
										<button onclick="abrirVentana('<?php echo base_url('inicio/editar/tamenaza/'.$value['id']) ?>')" class="btn btn-primary btn-sm">Editar</button>
										<button onclick="borrar('tamenaza', <?= $value['id'] ?>)" class="btn btn-danger btn-sm">Borrar</button>
								</tr>
								<?php } ?>
							</table> <br>
						</div>
						<div class="col-md-12 center" style="">
							<div class="row"><br>
								<div class="col-md-9">
								    <h5>Amenazas</h5>
							    </div>
							    <div class="col-md-3">
							    	<button class="btn btn-primary right btn-sm" title="Nueva Amenaza" onclick="abrirVentana('<?php echo base_url('inicio/agregar/subamenaza/') ?>')"><b>+ Nuevo</b></button>
							    </div>
						    </div>
							<table class="table-bordered table-hover table-striped" style="width: 100%">
								<tr>
									<th>N°</th>
									<th>Nombre</th>
									<th>Tipo</th>
									<th>Subipo</th>
									<th>Opciones</th>
								</tr>
								<?php $i=0;
								foreach ($amenaza as $value) { 
									foreach ($samenaza[$value['id']] as $key) { ?>
									<tr>
										<td><?= ++$i ?></td>
										<td><?php echo $key['nombre']; ?></td>
										<td><?php echo $value['nombre']; ?></td>
										<td><?php echo $key['tipo']; ?></td>
										<td>
											<button onclick="abrirVentana('<?php echo base_url('inicio/editar/subamenaza/'.$key['id']) ?>')" class="btn btn-primary btn-sm">Editar</button>
											<button onclick="borrar('subamenaza', <?= $value['id'] ?>)" class="btn btn-danger btn-sm">Borrar</button>
									</tr>
									<?php } 
								} ?>
							</table> <br>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">

	function borrar(tipo, id) {
		if(confirm('¿Esta seguro de Borrar el/la '+tipo+'? El cambio es irreversible.')){
			var xmlhttp;
			xmlhttp=new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {

				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				    // var last=xmlhttp.responseText;
				    alert('Elemento eliminado');
				}
			}
			xmlhttp.open("GET","<?php echo base_url('inicio/eliminar') ?>/"+tipo+'/'+id,true);
			xmlhttp.send();
		}
	}

</script>