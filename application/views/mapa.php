<!DOCTYPE html>
<html>
  <head>
    <title>Mapa de Noticias</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 500;
        width: 500;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .infonew{
        
      }
    </style>
  </head>
  <body>
      
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDdvYH2xHagCN1gxFTcrEDh4w7XkmnDeU8&callback=initMap" async defer></script>
    <script>

    var icons =new Array();
    var titles= new Array();
    var links=new Array();
    var lugares=new Array();
    var ids=new Array();
    var ni=0; //noticias 
    //var na=0; //noticias actual
    var nid=0;
    var lugar;

      function initMap() {
        //iniciar mapa
        map = new google.maps.Map(document.getElementById('map'), {
          minZoom: 12, 
          backgroundColor: 'purple',
          // mapTypeId: 'roadmap', 
          center: {lat: -16.4924556, lng: -68.1302557},
          zoom: 14 
        });   

        //marcadores para las noticias
        var markerN = new Array();

        <?php for ($i=1; $i <= 50; $i++) {  ?>
          markerN[<?= $i ?>] = new google.maps.Marker({
            // position: {lat: <?php  ?>, lng: <?php  ?>},
            title: "Noticia_<?= $i ?>",
            // label: 'M',
            icon: "http://maps.google.com/mapfiles/kml/pal3/icon33.png"
            // map: map
          });

          markerN[<?= $i ?>].addListener('click', function() {
            // map.setZoom(8);
            // map.setCenter(marker.getPosition());
            verinfo(<?= $i-1 ?>); 
          });

          <?php } ?>
          //al hacer click al mapa
          /*google.maps.event.addListener(map, 'click', function(event) {
            // addMarker(event.latLng, map, markerR, markerE);
            // alert(event.latLng);
            addMarker(map);
            // setInterval("noticias(map, markerN);",3000);
          });*/
          //colocar el mapa en el lugar de la noticia
          google.maps.event.addDomListener(
              // document.getElementsByName('noticia'),
              document.getElementById('noticias'),
              'click',
              function() {
                addMarker(map);
              }
          );
          //cargar las noticias al iniciar y cada x mins
          google.maps.event.addDomListener(window,'load',
              function() {
                noticias(map, markerN);
                setInterval(function() {
                  apilar();
                  noticias(map, markerN);
                }, 10000);
              }
          );
          //cargar las noticias cada x mins
          google.maps.event.addDomListener(document.getElementById('mins'),'change',
              function() {
                noticias(map, markerN);
              }
          );

        }

        // Adds a marker to the map.
      function addMarker(map) {
        // Add the marker at the clicked location, and add the next-available label
        // from the array of alphabetical characters.
        if(nid!=0){
          // alert(recojo);
          // var flightPlanCoordinates = [
          //   recojo,
          //   entrega,
          // ];
          // markerR.setPosition(recojo);
          // markerR.setMap(map);
          // markerE.setPosition(entrega);
          // markerE.setMap(map);
          map.panTo(lugares[nid]);
          // alert();
          // flightPath.setPath(flightPlanCoordinates);
          // flightPath.setMap(map);
          // alert('super');
        }
      }
      //colocar las noticias en el mapa
      function noticias(map, markerN) {

        for (var i = 0; i < ni; i++) {
          markerN[i+1].setPosition(lugares[i]);
          markerN[i+1].setTitle(titles[i]);
          markerN[i+1].setMap(map);
        };

      }

      // setInterval("initMap()",3000);

    </script>
    <section class="content">
      <div class="row" >
        <div id="news" class="col-md-2 center"  >
          <h4>Ultimos Eventos</h4>
          <div id="noticias" align="left" style="font-size: 14px; ">
            
          </div><br>
          <button id="prueba" class="btn btn-warning btn-sm btn-block">Otro Evento</button>
          <input class="form-control" id="mins" value=1 type="number">
          <br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br>
        </div>
        <div id="map" class="col-md-10"></div>
        <div id="info" class="hide">
          <h4>
            <span id="stitle"></span>
            
            </h4>
            <span id="simg"></span>

          <!-- <button class="btn btn-info" onclick="vermapa()">Ver en el mapa</button> -->
          <hr>

          <button class="btn btn-primary" onclick="vernew()">Ir a la noticia</button>
          <button class="btn btn-default" onclick="cerrar()">Cerrar</button>
        </div>
      </div>
    </section>
  </body>
</html>

<script type="text/javascript">

  function cerrar() {
    document.getElementById('info').className="hide";
    document.getElementById('map').className="col-md-10";
  }

  function verinfo(id) {
    nid=id;
    document.getElementById('stitle').innerHTML="<h4>"+titles[id]+"</h4>";
    document.getElementById('info').className="col-md-3";
    document.getElementById('map').className="col-md-7";
    document.getElementById('simg').innerHTML='<br><h5>Obteniendo Información...</h5>';
    var xmlhttp;
    xmlhttp=new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {

      if (xmlhttp.readyState==4 && xmlhttp.status==200) {
        document.getElementById('simg').innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","<?php echo base_url('inicio/dmapa') ?>/"+ids[id],true);
    xmlhttp.send();
    
  }

  function vernew() {
    abrirVentana("<?php echo base_url('inicio/detalles') ?>/"+ids[nid]);
  }

  function apilar() {
    var xmlhttp;
    xmlhttp=new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {

      if (xmlhttp.readyState==4 && xmlhttp.status==200) {
        var last=xmlhttp.responseText;
        // alert('actualizando'+last);
        newinfo=last.split('|');
        dangers="<ul>";
        for (var i = 0; i < newinfo.length; i++) {
          news=newinfo[i].split(';');
          icons[i+1]=news[0];
          titles[i+1]=news[1];
          ids[i+1]=news[2];
          coor=news[3].split(',');
          clat=parseFloat(coor[0]);
          clng=parseFloat(coor[1]);
          lugares[i+1] = {lat: clat, lng: clng};
          dangers=dangers+"<li><a href='#' name='noticia' onclick='vermapa("+(i+1)+")'>"+titles[i+1]+"</a></li>";
          document.getElementById('noticias').innerHTML=dangers+'</ul>';      
        };
        ni=i;
      }
    }
    xmlhttp.open("GET","<?php echo base_url('inicio/getnoticias') ?>",true);
    xmlhttp.send();
  }

  function vermapa(id) {
    
    nid=id;
    verinfo(id);
    // alert('ver'+id+lugar);
  }

  apilar();

  /*function tiempo() {
    mas=document.getElementById('mins').value;
    document.getElementById('mins').value=mas+1;
  }*/

  // setInterval('tiempo()', 3000);

</script>
