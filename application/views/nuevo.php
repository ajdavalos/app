<link href="<?php echo base_url() ?>css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url() ?>js/select2.min.js"></script>
<!-- para el mapa -->
<?php $itipo="" ?>
<script type="text/javascript">

	var codigos = new Array();
	var abrs = new Array();
	var tamz = new Array();

	var emed=new Array();
	var tmed=new Array();
	
	<?php foreach ($medida as $value) { 
		if($value['tipo']>1){ ?>
		emed[<?php echo $value['id'] ?>]="<?php echo $value['extra'] ?>"; <?php } ?>
		tmed[<?php echo $value['id'] ?>]=<?php echo $value['tipo'] ?>;
	<?php } ?>
	var orden=new Array();
	var medios=new Array();
	var lugares=new Array();
	var om=0;am=0;
	<?php foreach ($tmedio as $value) { ?>
		//por tipo
		medios[<?= $value['id'] ?>]=new Array();
		<?php foreach ($medio[$value['id']] as $valor) { ?>
			orden[om]=<?= $valor['id'] ?>;
			medios[<?= $value['id'] ?>][om]="<?= $valor['nombre'] ?>";
			lugares[om]="<?= $valor['lugar'] ?>";
			om++;
		<?php }
	} 

/*	foreach ($amenaza as $value) { ?>
		tamz[<?= $value['id'] ?>]="<?= $value['tipo'] ?>";
		<?php if($value['tipo']==2){ ?>
			samz[<?= $value['id'] ?>]=new Array();
			<?php foreach ($samenaza[$value['id']] as $key) { ?>
				ults[am]=<?= $key['id'] ?>;
				samz[<?= $value['id'] ?>][am]="<?= $key['nombre'] ?>";
				gamz[am]="<?= $key['tipo'] ?>";
				am++;
			<?php } 
			} ?>
	<?php } */ ?>

	$(document).ready(function() {
    $('.s2multiple').select2(
    		{
    		        placeholder: "Seleccionar una o mas opciones"
    		    }
    	);
	});
	

</script>
<body>
	<div class="container">
		<div class="row">

			<div class="col-md-2"></div>
			<div class="col-md-8">
				<br>
				<div id="page-title " >
					<div class="row">
						<div class="col-md-10">
						    <h2>Nueva Información</h2>
						    <p>Formulario de información de seguridad.</p>
					    </div>
					    <div class="col-md-2 right">
					    	<a href="<?php echo base_url() ?>">Inicio</a>
					    </div>
				    </div>
				</div>
				<div class="box">
				    <div class="box-body">
				        <h3 class="title-hero">
				            Datos
				        </h3>
				        <div class="example-box-wrapper">
				            <?php echo validation_errors();
                    				echo form_open_multipart('/inicio/nuevo'); ?>
				                <!-- aqui viene el formulario -->
				                <table class="table">
				                	<tr>
				                		<td width="25%">Títular:</td>
				                		<td width="75%"><input class="form-control" name="nombre" id="nombre" placeholder="Ingrese el títular de la información" required></td>
				                	</tr>
				                	<tr>
				                		<td>Amenaza: </td>
				                		<td><select  class="form-control single" name="amenaza" id="amenaza" onchange="famenaza()" required>
				                				<option value=0>...Indique el tipo de Amenaza</option>
				                				<?php foreach ($amenaza as $value) { ?>
				                					<option value=<?= $value['id'] ?> ><?= $value['nombre'] ?></option>
				                				<?php } ?>
				                			</select> 
				                			<?php $ltipo="";
				                			foreach ($amenaza as $value) { ?>
				                				<script type="text/javascript"> 
				                					codigos[<?= $value['id'] ?>]=<?= $value['last'] ?>;
				                					abrs[<?= $value['id'] ?>]="<?= $value['cod'] ?>";
				                					tamz[<?= $value['id'] ?>]=0;
				                				</script>
				                				<?php if(count($samenaza[$value['id']])>0){ ?>
				                				<script type="text/javascript"> 
				                					tamz[<?= $value['id'] ?>]=1;
				                				</script>
				                				<div id="damenaza<?= $value['id'] ?>" class="hide">
				                					<br>
					                				<select class="s2multiple form-control" name="" id="suba<?= $value['id'] ?>" style="width:100%;" multiple>
					                					<?php foreach ($samenaza[$value['id']] as $key) { 
					                						if($key['tipo']!=''&&$key['tipo']!=$ltipo){ 
					                							$ltipo=$key['tipo']; ?>
					                							</optgroup><optgroup label="<?= $key['tipo'] ?>">
					                						<?php } ?>
					                						<option value=<?= $key['id'] ?> ><?= $key['nombre'] ?></option>
					                					<?php } ?>
					                				</select>
				                					<div class="hide" id="dambito">
				                						<div class="col-md-4">
				                							<br>
				                							<label>Ambito del conflicto</label>
				                						</div>
				                						<div class="col-md-8"><br>
					                						<select class="form-control" name="ambito" id="ambito">
						                						<option value=1>Nacional</option>
						                						<option value=2>Regional</option>
						                						<option value=3>Departamental</option>
						                						<option value=4>Municipal</option>
						                						<option value=5>Vecinal/Comunal</option>
						                						<option value=6>Institucional</option>
						                					</select>
					                					</div>
					                				</div>
				                				</div>
				                			<?php } } ?>
				                			
				                			<div class="hide" id="dotraa"><br>
					                			<input class="form-control" name="otra" placeholder="Ingrese la Amenaza" >
					                		</div>
				                		</td>
				                	</tr>
				                	<tr>
				                		<td>Código:</td>
				                		<td><input class="form-control" name="codigo" id="codigo" readonly></td>
				                	</tr>
				                	<tr>
				                		<td>Estado:</td>
				                		<td><select  class="form-control single" name="estado" id="estado" onchange="festado()">
				                				<option value=0>Evento en Desarrollo</option>
				                				<option value=1>Evento Anunciado</option>
				                				<option value=2>Noticia Relacionada</option>
				                			</select>
				                		</td>
				                	</tr>
				                	<tr>
				                		<td>Fecha del Registro: </td>
				                		<td><input class="form-control" type="date" id="fecha" name="fecha" value="<?php echo date('Y-m-d') ?>" readonly></td>
				                	</tr>
				                	<tr>
				                		<td>Medio:</td>
				                		<td><select class="form-control single" name="tmedio" id="medio" onchange="fmedio()" placeholder="Seleccione un elemento">				                				
				                			<?php foreach ($tmedio as $value) {  ?>
				                				<option value=<?= $value['id'] ?>><?= $value['nombre'] ?></option>
				                			<?php } ?>
				                				<option value=0>Información Extraoficial</option>
				                			</select>

				                			<div id="dmedio">
				                				
				                			</div>
				                		</td>
				                	</tr>
				                </table>
				                <h4>Fuente</h4>
				                <table class="table">
				                	<tr>
				                		<td>Procedencia:</td>
				                		<td><select  class="form-control single" name="fuentep" id="fuentep" onchange="">
				                			<?php foreach ($fuente as $value) { ?>
				                				<option value=<?= $value['id'] ?>><?= $value['nombre'] ?></option>
				                			<?php } ?>
				                			</select>
				                		</td>
				                	</tr>
				                	<tr>
				                		<td>Valoración:</td>
				                		<td><select  class="form-control single" name="fuentev" id="fuentev" onchange="">
				                				<option value=1>Muy Confiable</option>
				                				<option value=2>Confiable</option>
				                				<option value=3>Medianamente Confiable</option>
				                				<option value=4>Poco Confiable</option>
				                				<option value=5>Desconfiable</option>
				                				<option value=6>Nunca se trabajo con la fuente</option>
				                			</select>
				                		</td>
				                	</tr>
				                </table>
				                <h4>Información</h4>
				                <table class="table">
				                	<tr>
				                		<td>Credibilidad: <i class="fa fa-help"></i></td>
				                		<td><select  class="form-control single" name="infoc" id="infoc" onchange="">
				                				<option value=1>Muy Creible</option>
				                				<option value=2>Creible</option>
				                				<option value=3>Medianamente Creible</option>
				                				<option value=4>Poco Creible</option>
				                				<option value=5>No Creible</option>
				                				<option value=6>Nunca se recibio este tipo de información</option>
				                			</select>
				                		</td>
				                	</tr>
				                	<tr>
				                		<td>Pertinencia: <i class="fa fa-help"></i></td>
				                		<td>
				                			<div class="row">
				                				<div class="col-md-5">
				                					Agencias de Cooperación: 
				                				</div>
				                				<div class="col-md-7">
					                				<select  class="form-control single" name="infop1" id="infop1" >
						                				<option value=1>Muy Pertinente</option>
						                				<option value=2>Pertinente</option>
						                				<option value=3>Medianamente Pertinente</option>
						                				<option value=4>Poco Pertinente</option>
						                				<option value=5>No Pertinente</option>
						                				<option value=0 selected>Se desconoce la Pertinencia</option>
						                			</select>
						                		</div>
						                		<div class="col-md-5">
				                					Bancos: 
				                				</div>
				                				<div class="col-md-7">
					                				<select  class="form-control single" name="infop2" id="infop2" >
						                				<option value=1>Muy Pertinente</option>
						                				<option value=2>Pertinente</option>
						                				<option value=3>Medianamente Pertinente</option>
						                				<option value=4>Poco Pertinente</option>
						                				<option value=5>No Pertinente</option>
						                				<option value=0 selected>Se desconoce la Pertinencia</option>
						                			</select>
						                		</div>
						                		<div class="col-md-5">
				                					Empresas Privadas: 
				                				</div>
				                				<div class="col-md-7">
					                				<select  class="form-control single" name="infop3" id="infop3" >
						                				<option value=1>Muy Pertinente</option>
						                				<option value=2>Pertinente</option>
						                				<option value=3>Medianamente Pertinente</option>
						                				<option value=4>Poco Pertinente</option>
						                				<option value=5>No Pertinente</option>
						                				<option value=0 selected>Se desconoce la Pertinencia</option>
						                			</select>
						                		</div>
						                		<div class="col-md-5">
				                					Otros: 
				                				</div>
				                				<div class="col-md-7">
					                				<select  class="form-control single" name="infop4" id="infop4" >
						                				<option value=1>Muy Pertinente</option>
						                				<option value=2>Pertinente</option>
						                				<option value=3>Medianamente Pertinente</option>
						                				<option value=4>Poco Pertinente</option>
						                				<option value=5>No Pertinente</option>
						                				<option value=0 selected>Se desconoce la Pertinencia</option>
						                			</select>
						                		</div>
				                			</div>
				                		</td>
				                	</tr>
				                </tABLE>
				                <div id="actores" class="hide">
				                <h4>Actores</h4>
				                <table class="table table-bordered">
				                	<tr>
				                		<th>Generadores del Conflicto / Parte Demandada</th>
				                		<th>Sector Afectado / Parte Demandante</th>
				                	</tr>
				                	<tr><td><label><input name="act0[]" id="" value=1 type="checkbox" > Gobierno Central</label></td>
				                		<td rowspan=2><label><input name="act1[]" id="scivil1" value=5 type="checkbox" onchange="mostrar('dscivil1')"> Sociedad Civil Organizada</label>
						                	<div class="hide" id="dscivil1">
						                		<select class="form-control" name="parte_5_1" style="width:100%;" >
						                				<?php foreach ($sector as $value) { ?>
						                					<option value=<?= $value['id'] ?>><?= $value['nombre'] ?></option>
						                				<?php } ?>
						                			</select>
						                		</div></tr>
				                	<tr><td><label><input  name="act0[]" id="regional" value=2 type="checkbox" onchange="mostrar('dregional0')" > Gobierno Regional</label>
				                		<div class="hide" id="dregional0">
				                			<select  class="form-control single" name="parte_2_0" id="regional0" >
				                				<?php foreach ($depr as $value) { ?>
				                					<option value=<?= $value['id'] ?>><?= $value['nombre'] ?></option>
				                				<?php } ?>
				                			</select>
				                		</div></td>
				                		<!-- <td>
				                		</td> -->
				                	</tr>
				                	<tr><td><label><input  name="act0[]" id="" value=3 type="checkbox" onchange="mostrar('dmunicipal0')" > Gobierno Municipal/Local</label>
				                		<div class="hide" id="dmunicipal0">
				                			<input class="form-control" name="parte_3_0" placeholder="Ingrese la Alcaldia" >
				                		</div>
				                	</td>
				                	<td>
				                	</td>
				                	</tr>
				                	  <tr><td><label><input  name="act0[]" id="" value=4 type="checkbox" onchange="mostrar('dempresa0')"> Empresa Privada</label>
				                		<div class="hide" id="dempresa0">
				                			<input class="form-control" name="parte_4_0" placeholder="Ingrese la Empresa" >
				                		</div>
				                	</td>
				                	<td><label><input  name="act1[]" id="empresa1" value=4 type="checkbox" onchange="mostrar('dempresa1')"> Empresa Privada</label>
				                	<div class="hide" id="dempresa1">
				                			<input class="form-control" name="parte_4_1" placeholder="Ingrese la Empresa" >
				                		</div>
				                	</td>
				                	</tr>
				                	<tr><td><label><input  name="act0[]" id="" value=6 type="checkbox" > Poder Legislativo</label></td>
				                	<tr><td><label><input  name="act0[]" id="" value=7 type="checkbox" > Poder Judicial</label></td>
				                	<td></td></tr>
				                	<tr><td><label><input  name="act0[]" id="persona0" value=9 type="checkbox" onchange="mostrar('dpersona0')" > Otros</label>
				                		<div class="hide" id="dpersona0">
				                			<input class="form-control" name="parte_9_0" placeholder="Ingrese el nombre del actor" >
				                		</div>
				                	</td>
				                	<td><label><input  name="act1[]" id="persona1" value=9 type="checkbox" onchange="mostrar('dpersona1')"> Otros</label>
				                	<div class="hide" id="dpersona1">
				                			<input class="form-control" name="parte_9_1" placeholder="Ingrese el nombre del actor" >
				                		</div>
				                	</td></tr>
				                </table>
				                <table class="table">
				                	<tr>
				                		<th>Tipo de Medida: </th>
				                		<td><select  class="form-control single" name="medida" id="medida" onchange="fmedida()">
				                				<option value=0>Ninguno</option>
				                				<?php foreach ($medida as $value) { ?>
				                					<option value=<?php echo $value['id'] ?>><?php echo $value['nombre'] ?></option>
				                				<?php } ?>
				                			</select>
				                			<div id="dmedida" class="hide">
					                				<div class="col-md-4" id="med1">
					                					<br>
					                					<input name="emedida" id="emedida" placeholder="Número de Personas" class="form-control" >
					                				</div>
					                				<div class="col-md-4" id="med2">
					                					<br>
					                					<b><font color="#fff"><a class="btn btn-danger btn-sm" onclick="susnmed()">- Menos</a><a class="btn btn-primary btn-sm" onclick="addnmed()">&nbsp+&nbspMas&nbsp&nbsp</a></font></b>
					                				</div>
					                				<div class="col-md-2" >
					                					<br>
					                					<label>Inicio: </label>
					                				</div>
					                				<div class="col-md-4" id="med3"><br>
					                					<!-- <input type="time" class="form-control"> -->
					                					<select class="form-control" name="inicio">
					                						<?php for ($i=0; $i < 24; $i++) {   ?>
					                						<option><?php echo sprintf("%'.02d", $i); ?>:00</option>
					                						<?php } ?>
					                					</select>
					                				</div>
					                				<div class="col-md-2" >
					                					<br>
					                					<label>Fin: </label>
					                				</div>
					                				<div class="col-md-4" id="med3"><br>
					                					<!-- <input type="time" class="form-control"> -->
					                					<select class="form-control" name="fin">
					                						<?php for ($i=0; $i < 24; $i++) {   ?>
					                						<option><?php echo sprintf("%'.02d", $i); ?>:00</option>
					                						<?php } ?>
					                					</select>
					                				</div>
				                				<div class="col-md-4" >
				                					<br>
				                					Ejecutado por: 
				                				</div>
				                				<div class="col-md-8" id="med2"><br>
				                					<select  class="form-control single" name="ejecutor" id="scivil1" >
				                						<option value=0>Parte Demandante/Sector Afectado</option>
						                				<?php foreach ($sector as $value) { ?>
						                					<option value=<?= $value['id'] ?>><?= $value['nombre'] ?></option>
						                				<?php } ?>
						                			</select>
				                				</div>
				                			</div>
				                		</td>
				                	</tr>
				                </table>
				                </div>
				                <h4>Noticia</h4>
				                <table class="table">
				                	<!-- <tr>
				                		<td>Resumen de la Noticia: </td>
				                		<td><textarea class="form-control"  readonly id="resumen" name="resumen"></textarea></td>
				                	</tr> -->
				                	<tr>
				                		<td>Fecha de la Noticia: </td>
				                		<td><input type="date" class="form-control" name="nfecha" id="nfecha" value="<?php echo date('Y-m-d') ?>"></td>
				                	</tr>
				                	<tr>
				                		<td>Lugar de la Noticia: <br><br><a class="btn btn-info" id="vermapa" onclick="abrirVentana('<?php echo base_url('inicio/geocodificacion/') ?>');" style="color: #fff; "><b>Buscar en el mapa</b></a></td>
				                		<td><input class="form-control" name="lugar" id="lugar" placeholder="Ingrese la dirección del evento" onchange="flugar()" required><br>
				                			<input class="form-control" name="coordenadas" id="punto" value="0,0" readonly></td>
				                	</tr>
				                	<tr>
				                		<td>Imagen de la noticia: <br><br></td>
				                		<td><input type="file" class="form-control" name="img"  accept="image/*" multiple data-show-upload="true" data-show-caption="true" data-show-preview="true"></td>
				                			<!-- <img class="img-thumbnail img-fluid img-responsive" style="max-width: 100%; and height: auto;" src="<?php echo base_url('img/noticias/0.png') ?>">  -->
				                	</tr> 
				                	<tr>
				                		<td>Comentario: </td>
				                		<td><textarea class="form-control" id="hecho" name="hecho" placeholder="Adicionar comentario"></textarea></td>
				                	</tr>
				                	<tr>
				                		<td>Enlace de Referencia: </td>
				                		<td><input class="form-control" id="link" name="link" ></td>
				                	</tr>
				                </table>
				                <div id="fip">
				                <h4>Probabilidad e Impacto</h4>
				                <table class="table">
				                	<tr>
				                		<th>Probabilidad de que<br>el evento se repita:</th>
				                		<td width="70%">
				                			<select  class="form-control single" name="prob" id="prob" onchange="">
				                				<option value=5>Muy Alta (80% - 100%)</option>
				                				<option value=4>Alta (60% - 80%)</option>
				                				<option value=3>Media (40% - 60%)</option>
				                				<option value=2>Poca (20% - 40%)</option>
				                				<option value=1>Ninguna (0% - 20%)</option>
				                			</select>
				                		</td>
				                	</tr>
				                	<?php foreach ($imp as $value) { 
				                		if($value['tipo']!=$itipo){ 
				                			$itipo=$value['tipo']; ?>
				                		</td>
				                	</tr>
				                	<tr>
				                		<th>Impacto <?= $value['tipo'] ?>:</th>
				                		<td>
				                	<?php } ?> 
				                		<label><input name="check[]" value="<?= $value['id'] ?>"  onchange="mostrar('dimp_<?= $value['id'] ?>')" type="checkbox" > <?= ucfirst($value['nombre']) ?></label><br>
				                		<?php if($value['es']==1){ ?>
				                		<div class="hide" id="dimp_<?= $value['id'] ?>">
				                			<input type="number" min=1 class="form-control" name="imp_<?= $value['id'] ?>" value=1 placeholder="Ingrese el número">
				                			<br><textarea class="form-control" placeholder="Ingrese los nombres de los <?= $value['nombre'] ?>" name="det_<?= $value['id'] ?>"></textarea><br>
				                		</div>
				                	<?php } } ?>
				                	</tr>
				                	<tr>
				                		<td width="30%">Valoración de Riesgo: </td>
				                		<td width="70%"><select  class="form-control single" name="riesgo" id="riesgo" onchange="">
				                				<option value=1>Muy Bajo</option>
				                				<option value=2>Bajo</option>
				                				<option value=3>Medio</option>
				                				<option value=4>Alto</option>
				                				<option value=5>Muy Alto</option>
				                			</select></td>
				                	</tr>
				                	<tr>
				                		<td width="30%">Parpadeo del evento en el mapa: </td>
				                		<td width="70%"><input type="button" class="btn btn-default" id="imp" value="Desactivado" onclick="fimp()">
				                			<input type="hidden" id="vimp" name="imp" value=0 >
				                		</td>
				                	</tr>
				                </table>
				                </div>
				                <p class="center">
				                	<button class="btn btn-primary btn-lg " id="enviar" disabled >Enviar <i class="fa fa-send"></i></button>
				                	<a class="btn btn-lg btn-default" onclick="inicio()">Cancelar</a>
				                </p>
				            </form>
				        </div>
				    </div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</body>

<script type="text/javascript">

	var cmed=1;
	nmeds=new Array();
	nmeds[1]='menos de 50 personas';
	nmeds[2]='50 - 100 personas';
	nmeds[3]='100 - 500 personas';
	nmeds[4]='500 - 1000 personas';
	nmeds[5]='más de 1000 personas';
	
	function fmedio() {
		med=document.getElementById('medio').value;
		var info=""; var llugar="";
		if(med!=0){
			info='<br><select class="form-control" name="medio" >';
			for (var i = 0; i < medios[med].length; i++) {
				if(medios[med][i]!=undefined){
					if(lugares[i]!=''&&lugares[i]!=llugar)
						info=info+'</optgroup><optgroup label="'+lugares[i]+'">';
					llugar=lugares[i];
					info=info+'<option value="'+orden[i]+'">'+medios[med][i]+'</option>';
				}
			};
			info=info+'</optgroup></select>';
		}
		document.getElementById('dmedio').innerHTML=info;
	}
	
	fmedio();
	//funcion para mostrar un div
	function mostrar(idiv) {

		cdiv=document.getElementById(idiv).className;
		if(cdiv=="hide"){
			document.getElementById(idiv).className="";
		}else{
			document.getElementById(idiv).className="hide";
		}
	}
	//amenaza
	function famenaza() {
		// alert('hola'); 
		var info="";
		var lgroup="";
		amz=document.getElementById('amenaza').value;
		document.getElementById('enviar').disabled=false;
		document.getElementById('dotraa').className='hide';
		<?php foreach ($amenaza as $value) { 
			if(count($samenaza[$value['id']])>0){ ?>
			document.getElementById("damenaza<?= $value['id'] ?>").className="hide";
			document.getElementById("suba<?= $value['id'] ?>").name="";
		<?php }} ?>
		if(amz==0)
			document.getElementById('enviar').disabled=true;
		else{
			document.getElementById('codigo').value=abrs[amz]+'-'+codigos[amz];
			
			if(tamz[amz]==1){
				document.getElementById("damenaza"+amz).className="";
				document.getElementById("suba"+amz).name="subs[]";
			}
		}
		if(amz==4){
			document.getElementById('actores').className="";
			document.getElementById('dambito').className="row";
		}	else{
			document.getElementById('dambito').className="hide";
			document.getElementById('actores').className="hide";
		}
			
		// document.getElementById('damenaza').innerHTML=info;
	}
	function fmedida() {

		med=document.getElementById('medida').value;
		if(med!=0){
			document.getElementById('dmedida').className="row";
			if(tmed[med]>1){
				document.getElementById('emedida').placeholder=emed[med];
				document.getElementById('emedida').className="form-control";
			}else{
				document.getElementById('emedida').className="hide";
			}
			if(tmed[med]==2){
				// document.getElementById('emedida').className="";
				document.getElementById('med1').className="col-md-8";
				document.getElementById('med2').className="col-md-4";
				document.getElementById('emedida').value='menos de 50 personas.';
				cmed=1;
			}else{
				// document.getElementById('emedida').className="form-control";
				document.getElementById('med1').className="col-md-12";
				document.getElementById('med2').className="hide";
				document.getElementById('emedida').value='';
			}
		}else{
			document.getElementById('dmedida').className="hide";
		}
	}
	function addnmed () {
		if(cmed<5){
			cmed++;
			document.getElementById('emedida').value=nmeds[cmed];	
		}
	}
	function susnmed () {
		if(cmed>1){
			cmed--;
			document.getElementById('emedida').value=nmeds[cmed];	
		}
	}
	
	function abrirVentana(url) {
        window.open(url, "nuevo", "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=800, height=640");
        // verif();      
    }
    function festado() {
    	est=document.getElementById('estado').value;
    	if(est==2){
    		document.getElementById('fip').className="hide";
    		document.getElementById('lugar').disabled=true;
    		document.getElementById('vermapa').className="hide";
    	}else{
    		document.getElementById('fip').className="";
    		document.getElementById('lugar').disabled=false;
    		document.getElementById('vermapa').className="btn btn-info";
    	}
    }
    function fimp () {
    	climp=document.getElementById('imp').className;
    	if(climp=="btn btn-default"){
    		document.getElementById('imp').className="btn btn-info";
    		document.getElementById('vimp').value=1;
    		document.getElementById('imp').value="Activado";
    	}else{
    		document.getElementById('imp').className="btn btn-default";
    		document.getElementById('vimp').value=0;
    		document.getElementById('imp').value="Desactivado";
    	}
    }

</script>
