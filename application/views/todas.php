<body>
	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<br>
					<div class="row">
						<div class="col-md-10">
						    <h4>Todas las informaciones de seguridad</h4>
					    </div>
					    <div class="col-md-2 right">
					    	<a href="<?php echo base_url() ?>">Inicio</a>
					    </div>
				    </div><br>
				<table class="table">
					<tr>
						<th>Codigo</th>
						<th>Fecha</th>
						<th>Titular</th>
						<th>Ver</th>
						<th>Ir</th>
					</tr>
					<?php foreach ($notis as $value) { ?>
					<tr>
						<td><?php echo $value['codigo'] ?></td>
						<td><?php echo date('d/m/Y', strtotime($value['fecha'])) ?></td>
						<td><?php echo $value['nombre'] ?></td>
						<td><button onclick="abrirVentana('<?php echo base_url('inicio/detalles/'.$value['id']) ?>')" class="btn btn-info">Ver</button></td>
						<td><a href="<?php echo base_url('inicio/ver/'.$value['id']) ?>" class="btn btn-primary">Ir</a></td>
					</tr>
					<?php } ?>
				</table>
			</div>
			</div>
			<div class="col-md-2"></div>
			</div>
		</div>
	</div>
</body>
