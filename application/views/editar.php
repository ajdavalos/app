<?php if(isset($tmedio)){
	$medios=array();
	foreach ($tmedio as $value) {
		$medios[$value['id']]=$value['nombre'];
	}
} ?>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<br>
					<div class="row">
						<div class="col-md-10">
						    <h4>Editar 
						    	<?php switch ($tipo) {
						    		case 'tmedio':
						    			echo 'Tipo de Medio';
						    			break;
						    		case 'impactos':
						    			echo 'Impacto';
						    			break;
						    		default:
						    			echo ucwords($tipo);
						    			break;
						    	} ?></h4>
						    <!-- <p>Formulario de información de seguridad.</p> -->
					    </div>
					    <div class="col-md-2 right">
					    	<button class="btn btn-danger btn-sm" onclick="window.close();">X</button>
					    </div>
				    </div>
				<div>
					<form class="form-horizontal bordered-row" action="<?php echo base_url('inicio/editar/'.$tipo.'/'.$edit['id']) ?>" method="post">
						<label>Nombre: </label>
						<input class="form-control" name="nombre" placeholder="Ingrese el nombre" value="<?php echo $edit['nombre'] ?>" required>
						<?php if($tipo=='medio'){ ?>
							<label>Tipo: </label>
							<input class="form-control" value="<?php echo $medios[$edit['tipo']] ?>" disabled>
							<label>Departamento: </label>
							<select class="form-control single" name="lugar" id="lugar" >
								<option>Ninguno</option>
								<?php foreach ($depr as $value) { ?>
									<option value=<?= $value['id'] ?> ><?= $value['nombre'] ?></option>
								<?php } ?>
							</select>
							<script type="text/javascript">
								document.getElementById('lugar').value=<?php echo $edit['lugar'] ?>
							</script>
						<?php }
						if($tipo=='amenaza'){ ?>
							<label>Abrebiacion: </label>
							<input class="form-control" name="cod" placeholder="Ingrese la abrebiacion" required>
						<?php } 
						if($tipo=='subamenaza'){ ?>
							<label>Tipo de Amenaza: </label>
							<select class="form-control single" name="amenaza" id="amenaza" >
								<?php foreach ($amz as $value) { ?>
									<option value=<?= $value['id'] ?> ><?= $value['nombre'] ?></option>
								<?php } ?>
							</select>
							<label>Subtipo de Amenaza: </label>
							<select class="form-control single" name="tipo" id="tipo" >
								<option>Ninguno</option>
								<?php foreach ($tamz as $value) { ?>
									<option value=<?= $value['id'] ?> ><?= $value['nombre'] ?></option>
								<?php } ?>
							</select>
							<script type="text/javascript">
								document.getElementById('amenaza').value=<?php echo $edit['amenaza'] ?>;
								document.getElementById('tipo').value=<?php echo $edit['tipo'] ?>;
							</script>
						<?php } if($tipo=='impactos'){ ?>
							<label>Tipo de Impacto: </label>
							<select class="form-control single" name="tipo" id="tipo" >
								<?php foreach ($timp as $value) { ?>
									<option value=<?= $value['id'] ?> ><?= $value['nombre'] ?></option>
								<?php } ?>
							</select>
							<script type="text/javascript">
								document.getElementById('tipo').value=<?php echo $edit['tipo'] ?>;
							</script>
						<?php }if($tipo=='medida'){ ?>
							<label>Detalles: </label>
							<select class="form-control single" name="tipo" id="tipo" >
								<option value=1>No colocar detalles</option>
								<option value=2>Colcoar medidor de personas</option>
								<option value=3>Colocar texto de detalles</option>
							</select>
							<label>Texto de Sugerencia: </label>
							<input class="form-control" value="<?php echo $edit['extra'] ?>" name="extra" id="extra">
							<script type="text/javascript">
								document.getElementById('tipo').value=<?php echo $edit['tipo'] ?>;
							</script>
						<?php } ?>
						<br>
						<div class="row">
							<div class="col-md-6">
								<button type="reset" class="btn btn-info">Restablecer</button>
							</div>
							<div class="col-md-6" align="right">
								<button class="btn btn-default" onclick="window.close()" align="right">Cancelar</button>
								<button type="submit" class="btn btn-primary" align="right">Guardar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-1"></div>
			</div>
		</div>
	</div>
</body>
