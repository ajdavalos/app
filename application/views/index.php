<body>
	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<br>
				<h4>Información de Seguridad</h4>
				<hr>
				<p class="center">
					<a href="<?php echo base_url('inicio/nuevo') ?>">
						<button class="btn btn-primary center btn-block">Nueva Informacion</button>
					</a><br>
					<a href="<?php echo base_url('inicio/todas') ?>">
						<button class="btn btn-success center btn-block">Ver Todas las Informaciones</button>
					</a><br>
					<!-- <a href="<?php echo base_url('inicio/denuncias') ?>">
						<button class="btn btn-primary center btn-block">Ver Denuncias Pendientes</button>
					</a> -->
					<a href="<?php echo base_url('inicio/formulario') ?>">
						<button class="btn btn-primary center btn-block">Editar información del formulario</button>
					</a><br>
					<a href="<?php echo base_url('inicio/mapa') ?>">
						<button class="btn btn-info center btn-block">Ver Incidentes en el Mapa</button>
					</a>
				</p>
				
			</div>
			</div>
			<div class="col-md-2"></div>
			</div>
		</div>
	</div>
</body>
			