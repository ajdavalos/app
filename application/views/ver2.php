<?php $datos=$_POST; 
$dias[1]="lunes";
$dias[2]="martes";
$dias[3]="miercoles";
$dias[4]="jueves";
$dias[5]="viernes";
$dias[6]="sabado";
$dias[7]="domingo";

$meses[1]="enero";
$meses[2]="febrero";
$meses[3]="marzo";
$meses[4]="abril";
$meses[5]="mayo";
$meses[6]="junio";
$meses[7]="julio";
$meses[8]="agosto";
$meses[9]="septiembre";
$meses[10]="octubre";
$meses[11]="noviembre";
$meses[12]="diciembre";

$caso="";
$prota="";
$anta="";

?>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<br>
				<h4>Ver Información de Seguridad</h4>
				<table class="table">
					<tr>
						<td width="30%">Nombre:</td>
						<td width="70%"><?php echo $datos['nombre'] ?></td>
					</tr>
					<tr>
						<td>Amenaza: </td>
						<td>
								<?php $amz=array();
								$amz[1]='Terrorismo'; 
								$amz[2]='Conflicto Armado'; 
								$amz[3]='Crimen: '; 
								$amz[4]='Conflictividad Social: '; 
								$amz[5]='Peligros: '; 
								$amz[6]='Otros: '; 
								echo $amz[$datos['amenaza']];
								//amenazas
								$peligros = array();
								$peligros[1]='Accidente de Transito';
								$peligros[2]='Inundación';
								$peligros[3]='Incendio';
								$peligros[4]='Desastre Natural';
								$peligros[5]='Accidente Aereo';
								//conflictos
								$social=array();
								$social[1]="Reproducción Social/Acceso al Trabajo";
								$social[2]="Reproducción Social/Acceso a la Salud";
								$social[3]="Reproducción Social/Acceso a la Educación";
								$social[4]="Reproducción Social/Movilidad Social";
								$social[5]="Reproducción Social/Asuntos Economicos";
								$social[21]="Conflicto Cultural/Ideologico Político";
								$social[22]="Conflicto Cultural/Seguridad Ciudadana";
								$social[23]="Conflicto Cultural/Asuntos Etnicos";
								$social[24]="Conflicto Cultural/Asuntos Religioso Culturales";
								$social[25]="Conflicto Cultural/Asuntos de Genero";
								$social[26]="Conflicto Cultural/Asuntos Socioambientales";
								$social[11]="Conflicto Estructural/Leyes";
								$social[12]="Conflicto Estructural/Politicas";
								$social[13]="Conflicto Estructural/Procedimientos";
								//crimenes
								$crimen=array();
								$crimen[1]="Delincuencia Comun/Robo a Transeunte";
								$crimen[2]="Delincuencia Comun/Agresion Sexual";
								$crimen[11]="Crimen Organizado/Trata y Trafico de Personas";
								$crimen[12]="Crimen Organizado/Robo de Domicilios";
								$crimen[13]="Crimen Organizado/Robo de Vehiculos";
								$crimen[14]="Crimen Organizado/Robo de Autopartes";
								$crimen[15]="Crimen Organizado/Narcotrafico";
								$crimen[16]="Crimen Organizado/Asaltos y Atracos";
								//otros
								$otros=array();
								$otros[1]="Daño Reputacional";
								$otros[2]="Desaparición de Personas"; 
								$caso=$amz[$datos['amenaza']];
								if($datos['amenaza']==3){
									echo $caso=$crimen[$datos['crimen']];
								}if($datos['amenaza']==4){
									echo $social[$datos['social']];
								}if($datos['amenaza']==5){
									echo $caso=$peligros[$datos['peligro']];
								}if($datos['amenaza']==6){
									if($datos['otro']!=0){
										echo $caso=$otros[$datos['otro']];
									}else{
										echo $caso=$datos['otra'];
									}
								} ?>
							
							<div id="damenaza" >
								
							</div>
						</td>
					</tr>
					<tr>
						<td>Codigo:</td>
						<td><?php echo $datos['codigo'] ?></td>
					</tr>
					<tr>
						<td>Estado:</td>
						<td><?php if($datos['estado']==0) echo 'Evento en Desarrollo';
									else echo 'Evento Anunciado' ?>
						</td>
					</tr>
					<tr>
						<td>Fecha de la Notificación</td>
						<td><?php echo date('d/m/Y', strtotime($datos['fecha'])) ?></td>
					</tr>
					<tr>
						<td>Medio:</td>
						<td><?php $medio=array();
								$medio['1']='Television';
								$medio['2']='Escrito/Digital';
								$medio['3']='Red Social';
								$medio['4']='Radio';
								$medio['5']='Información Extraoficial';
								echo $medio[$datos['medio']];
								$medios=array();
								//medios de tv
								$medios[1][1]="Red Uno";
								$medios[1][2]="ATB";
								$medios[1][3]="Unitel";
								$medios[1][4]="PAT";
								//$medios escritos
								$medios[2][1]="El Diario";
								$medios[2][2]="La Razon";
								$medios[2][3]="El Extra";
								//red social
								$medios[3][1]="Facebook";
								$medios[3][2]="Twitter";
								$medios[3][3]="Watsapp";
								//Radio
								$medios[4][1]="Panamericana";
								$medios[4][2]="Fides";
								if(isset($datos['emedio'])) echo '<br>'.$medios[$datos['medio']][$datos['emedio']] ?>
						</td>
					</tr>
				</table>
				<h4>Fuente (4)</h4>
				<table class="table">
					<tr>
						<td>Procedencia:</td>
						<td><?php $prod=array();
								$prod[0]='Gobierno';
								$prod[1]='Otras Fuentes Oficiales del Estado';
								$prod[2]='Cuerpo Dip, Org. Int, ONGs';
								$prod[3]='Empresa Privada';
								$prod[4]='Sociedad Civil';
								echo $prod[$datos['fuentep']]; ?>
								<!-- <option value=5></option>
								<option value=6></option> -->
							</select>
						</td>
					</tr>
					<tr>
						<td>Valoración:</td>
						<td><?php $vr=array();
								$vr[1]='Muy Confiable';
								$vr[2]='Confiable';
								$vr[3]='Medianamente Confiable';
								$vr[4]='Poco Confiable';
								$vr[5]='Desconfiable';
								$vr[6]='Nunca se trabajo con la fuente';
								echo $vr[$datos['fuentev']] ?>
						</td>
					</tr>
				</table>
				<h4>Información (5)</h4>
				<table class="table">
					<tr>
						<td>Credibilidad: <i class="fa fa-help"></i></td>
						<td><?php $cred=array();
								$cred[1]='Muy Creible';
								$cred[2]='Creible';
								$cred[3]='Medianamente Creible';
								$cred[4]='Poco Creible';
								$cred[5]='No Creible';
								$cred[6]='Nunca se recibio este tipo de información'; 
								echo $cred[$datos['infoc']]  ?>
						</td>
					</tr>
					<tr>
						<td>Pertinencia: <i class="fa fa-help"></i></td>
						<td><?php $pert=array();
								$pert[1]='Muy Pertinente';
								$pert[2]='Pertinente';
								$pert[3]='Medianamente Pertinente';
								$pert[4]='Poco Pertinente';
								$pert[5]='No Pertinente';
								$pert[6]='Se desconoce la Pertinencia';
								echo $pert[$datos['infop']] ?>
						</td>
					</tr>
				</tABLE>
				<h4>Actores (8)</h4>
				<table class="table">
					<tr><th>Generadores del Conflicto / Parte Demandada</th></tr>
					<tr><td><?php $act0=array();
						 $act0[1]='Gobierno Central, ';
						 $act0[2]='Gobierno Regional ';
						 $act0[3]='Gobierno Municipal ';
						 $act0[4]='Empresa Privada ';
						 $act0[5]='Sociedad Civil Organizada (';
						 $act0[6]='Sociedad Civil No Organizada, ';
						 $act0[7]='Persona Natural (';
						 $dep=array();
						 $dep[1]='La Paz';
						 $dep[2]='Santa Cruz';
						 $dep[3]='Cochabamba';
						 $dep[4]='Oruro';
						 $dep[5]='Tarija';
						 $dep[6]='Sucre';
						 $dep[7]='Beni';
						 $dep[8]='Potosi';
						 $dep[9]='Pando';
						 $sector=array();
						 $sector[1]='Sector Salud';
						 $sector[2]='Sector Educacion';
						 $sector[3]='Gremiales';
						 $sector[4]='Sector Transporte';
						 $sector[5]='Juntas Vecinales';
						 $sector[6]='Mineros';
						 if(isset($datos['act0']))
							 for ($i=0; $i < count($datos['act0']); $i++) { 
							 	echo $anta.=$act0[$datos['act0'][$i]];
							 	if($datos['act0'][$i]==2){
							 		echo $anta.='de '.$dep[$datos['regional0']].', ';
							 	}
							 	if($datos['act0'][$i]==3){
							 		echo $anta.='de '.$datos['municipal0'].', ';
							 	}
							 	if($datos['act0'][$i]==4){
							 		echo $anta.=$datos['empresa0'].', ';
							 	}
							 	if($datos['act0'][$i]==5){
							 		echo $anta.=$sector[$datos['scivil0']].'), ';
							 	}
							 	if($datos['act0'][$i]==7){
							 		echo $anta.=$datos['persona0'].'), ';
							 	}
							 }
					 ?>
					</td></tr>
					<tr><th>Sector Afectado / Parte Demandante</th></tr>
					<tr><td><?php if(isset($datos['act1']))
						for ($i=0; $i < count($datos['act1']); $i++) { 
						 	echo $prota.=$act0[$datos['act1'][$i]];
						 	if($datos['act1'][$i]==2){
						 		echo $prota.='de '.$dep[$datos['regional1']].', ';
						 	}
						 	if($datos['act1'][$i]==3){
						 		echo $prota.='de '.$datos['municipal1'].', ';
						 	}
						 	if($datos['act1'][$i]==4){
						 		echo $prota.=$datos['empresa1'].', ';
						 	}
						 	if($datos['act1'][$i]==5){
						 		echo $prota.=$sector[$datos['scivil1']].'), ';
						 	}
						 	if($datos['act1'][$i]==7){
						 		echo $prota.=$datos['persona1'].'), ';
						 	}
						 } ?>
					</td></tr>
				</table>
				<table class="table">
					<tr>
						<th>Tipo de Medida: </th>
						<td><?php $tmedida=array();
								$tmedida[0]='Ninguno';
								$tmedida[1]='Manifestación Social';
								$tmedida[2]='Paro de Transporte';
								$tmedida[3]='Toma de Instalaciones';
								$tmedida[4]='Huelga';
								$tmedida[5]='Paro Civico';
								$tmedida[6]='Otro';
							echo $tmedida[$datos['medida']]; ?>
						</td>
					</tr>
				</tABLE>
				<h4>Noticia </h4>
				<table class="table">
					<tr>
						<td width="30%">Valoración de Riesgo: </td>
						<td width="70%"><?php $riesgo=array(); 
								$riesgo[1]='Muy Bajo';
								$riesgo[2]='Bajo';
								$riesgo[3]='Medio';
								$riesgo[4]='Alto';
								$riesgo[5]='Muy Alto';
							echo $riesgo[$datos['riesgo']] ?></td>
					</tr>
					<tr>
						<td>Resumen de la Noticia: </td>
						<td align="justify">El día <?php $fecha=strtotime($datos['nfecha']);
						echo $dias[date('w', $fecha)].', '.date('d', $fecha).' de '.$meses[date('n', $fecha)].' de '.date('Y', $fecha);
						echo ' en el/la '.$datos['lugar'] ?> se dio lugar un caso de <?php echo $caso; ?>. 
						
						<?php if($prota!=""){ ?>
						El hecho fue protagonizado por <?php echo $prota ?> en contra del/de la <?php echo $anta ?> y 
						<?php }if(isset($datos['muertos'])||isset($datos['heridos'])||isset($datos['vehiculos'])){ ?>
						tubo como resultado <?php if(isset($datos['heridos'])) echo $datos['nheridos'].' Herido(s), ';
						if(isset($datos['muertos'])) echo $datos['nmuertos'].' Muerto(s)'; 
						if(isset($datos['vehiculos'])||isset($datos['instalaciones'])){ ?>
							Causando daño a 
							<?php if(isset($datos['vehiculos'])) echo 'Vehiculos, ';
							if(isset($datos['instalaciones'])) echo 'Instalaciones';
						} ?> y generando 
						<?php if(isset($datos['io1'])) echo 'Cierre de Oficianas, ';
						if(isset($datos['io2'])) echo 'Intransitabilidad Urbana, ';
						if(isset($datos['io3'])) echo 'Bloqueo de Carreteras'; } ?>.
						<?php echo $datos['hecho'] ?>
						</td>
					</tr>
					<tr>
						<td>Lugar de la Noticia: </td>
						<td><?php echo $datos['lugar'] ?></td>
					</tr>
					<tr>
						<td>Ubicación en el Mapa: </td>
						<td><?php echo $datos['coordenadas'] ?></td>
					</tr>
					<tr>
						<td>Fecha de la Noticia: </td>
						<td><?php echo date('d/m/Y',$fecha) ?>
						</td>
					</tr>
					<!-- <tr>
						<td>Descripción del hecho: </td>
						<td><?php echo $datos['hecho'] ?></td>
					</tr> -->
					<tr>
						<td>Enlace de Referencia: </td>
						<td><a href="<?php echo $datos['link'] ?>"><?php echo $datos['link'] ?></a></td>
					</tr>
				</table>
				<h4>Probabilidad e Impacto</h4>
				<table class="table">
					<tr>
						<th>Probabilidad de que el evento se repita:</th>
						<td><?php $prob=array();
								$prob[5]='Muy Alta (80% - 100%)';
								$prob[4]='Alta (60% - 80%)';
								$prob[3]='Media (40% - 60%)';
								$prob[2]='Poca (20% - 40%)';
								$prob[1]='Ninguna (0% - 20%)';
							echo $prob[$datos['prob']] ?>
						</td>
					</tr>
					<tr>
						<th>Impacto Personal:</th>
						<td><?php if(isset($datos['heridos'])) echo $datos['nheridos'].' Herido(s), ' ?>
							<?php if(isset($datos['muertos'])) echo $datos['nmuertos'].' Muerto(s)' ?>
						</td>
					</tr>
					<tr>
						<th>Impacto en Bienes o Instalaciones:</th>
						<td>
							<?php if(isset($datos['vehiculos'])) echo 'Vehiculos, ' ?>
							<?php if(isset($datos['instalaciones'])) echo 'Instalaciones' ?>
						</td>
					</tr>
					<tr>
						<th>Impacto Operacional:</th>
						<td>
							<?php if(isset($datos['io1'])) echo 'Cierre de Oficianas, ' ?>
							<?php if(isset($datos['io2'])) echo 'Intransitabilidad Urbana, ' ?>
							<?php if(isset($datos['io3'])) echo 'Bloqueo de Carreteras ' ?>
							<?php //if(isset($datos['io4'])) echo 'Intransitabilidad Urbana' ?>
						</td>
					</tr>
				</table>
				<a href="<?php echo base_url('inicio/nuevo') ?>">
					<button class="btn btn-primary">Nueva Informacion</button>
				</a>
			</div>
			<div class="col-md-2"></div>
			</div>
		</div>
	</div>
</body>
