<?php if(isset($tmedio)){
	$medios=array();
	foreach ($tmedio as $value) {
		$medios[$value['id']]=$value['nombre'];
	}
} ?>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<br>
					<div class="row">
						<div class="col-md-10">
						    <h4>Agregar 
						    	<?php switch ($tipo) {
						    		case 'tmedio':
						    			echo 'Tipo de Medio';
						    			break;
						    		case 'impactos':
						    			echo 'Impacto';
						    			break;
						    		default:
						    			echo ucwords($tipo);
						    			break;
						    	} ?></h4>
						    <!-- <p>Formulario de información de seguridad.</p> -->
					    </div>
					    <div class="col-md-2 right">
					    	<button class="btn btn-danger btn-sm" onclick="window.close();">X</button>
					    </div>
				    </div>
				<div>
					<form class="form-horizontal bordered-row" action="agregar/<?= $tipo.'/'.$ext ?>" method="post">
						<label>Nombre: </label>
						<input class="form-control" name="nombre" placeholder="Ingrese el nombre" required>
						<?php if($tipo=='medio'){ ?>
							<label>Tipo: </label>
							<input class="form-control" value="<?php echo $medios[$ext] ?>" disabled>
							<input type="hidden" name="tipo" placeholder="Ingrese el nombre" value="<?php echo $ext ?>" required>
							<label>Departamento: </label>
							<select class="form-control single" name="lugar"  >
								<option>Ninguno</option>
								<?php foreach ($depr as $value) { ?>
									<option value=<?= $value['id'] ?> ><?= $value['nombre'] ?></option>
								<?php } ?>
							</select>
						<?php }
						if($tipo=='amenaza'){ ?>
							<label>Abrebiacion: </label>
							<input class="form-control" name="cod" placeholder="Ingrese la abrebiacion" required>
						<?php } 
						if($tipo=='subamenaza'){ ?>
							<label>Tipo de Amenaza: </label>
							<select class="form-control single" name="amenaza"  >
								<?php foreach ($amz as $value) { ?>
									<option value=<?= $value['id'] ?> ><?= $value['nombre'] ?></option>
								<?php } ?>
							</select>
							<label>Subtipo de Amenaza: </label>
							<select class="form-control single" name="tipo"  >
								<option>Ninguno</option>
								<?php foreach ($tamz as $value) { ?>
									<option value=<?= $value['id'] ?> ><?= $value['nombre'] ?></option>
								<?php } ?>
							</select>
						<?php } if($tipo=='impactos'){ ?>
							<label>Tipo de Impacto: </label>
							<select class="form-control single" name="tipo"  >
								<?php foreach ($timp as $value) { ?>
									<option value=<?= $value['id'] ?> ><?= $value['nombre'] ?></option>
								<?php } ?>
							</select>
						<?php }if($tipo=='medida'){ ?>
							<label>Detalles: </label>
							<select class="form-control single" name="tipo"  >
								<option value=1>No colocar detalles</option>
								<option value=2>Colcoar medidor de personas</option>
								<option value=3>Colocar texto de detalles</option>
							</select>
							<label>Texto de Sugerencia: </label>
							<input class="form-control" value="" name="extra">
						<?php } ?>
						<br>
						<p align="right"><button class="btn btn-default" onclick="window.close()">Cancelar</button>
						<button type="submit" class="btn btn-primary">+ Agregar</button></p>
						
						
					</form>
				</div>
			</div>
			<div class="col-md-1"></div>
			</div>
		</div>
	</div>
</body>
