<!DOCTYPE html>
<html lang="en">
<head>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> Nueva Información </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- iconos -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
<!-- Bootstrap 4.0 -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/estilo.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
	//para abrir en una nueva ventana
	function abrirVentana(url) {
        window.open(url, "nuevo", "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=800, height=640");
        // verif();      
    }
    //para regresar a la vista anterior
    function regresar() {
    	window.history.back();
    }
    //para volver a inicio
    function inicio() {
    	window.location="<?php echo base_url() ?>";
    }

</script>
<?php date_default_timezone_set('America/La_Paz');  ?>