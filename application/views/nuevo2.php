<?php date_default_timezone_set('America/La_Paz'); ?>
<body>
	<div class="container">
		<div class="row">

			<div class="col-md-2"></div>
			<div class="col-md-8">
				<br>
				<div id="page-title">
				    <h2>Nueva Información</h2>
				    <p>Formulario de información de seguridad.</p>
				</div>
				<div class="box">
				    <div class="box-body">
				        <h3 class="title-hero">
				            Datos (1,2,3, 6,7)
				        </h3>
				        <div class="example-box-wrapper">
				            <form class="form-horizontal bordered-row" action="ver" method="post">
				                <!-- aqui va el formulario -->
				                <table class="table">
				                	<tr>
				                		<td width="30%">Nombre:</td>
				                		<td width="70%"><input class="form-control" name="nombre" id="nombre" placeholder="Ingrese el nombre de la información" required></td>
				                	</tr>
				                	<tr>
				                		<td>Amenaza: </td>
				                		<td><select  class="form-control single" name="amenaza" id="amenaza" onchange="dame()" required>
				                				<option value=0>...Indique el tipo de Amenaza</option>
				                				<option value=1>Terrorismo <!-- (T) --></option>
				                				<option value=2>Conflicto Armado<!-- (CA) --></option>
				                				<option value=3>Crimen <!-- (Cr) --></option>
				                				<option value=4>Conflictividad Social <!-- (CS) --></option>
				                				<option value=5>Peligros <!-- (P) --></option>
				                				<option value=6>Otros <!-- (O) --></option>
				                			</select>

				                			<div id="damenaza" >
				                				<!-- Hola -->
				                			</div>
				                			<div class="hide" id="dotraa"><br>
					                			<input class="form-control" name="otra" placeholder="Ingrese la Amenaza" >
					                		</div>
				                		</td>
				                	</tr>
				                	<tr>
				                		<td>Codigo:</td>
				                		<td><input class="form-control" name="codigo" id="codigo" readonly></td>
				                	</tr>
				                	<tr>
				                		<td>Estado:</td>
				                		<td><select  class="form-control single" name="estado" id="estado" onchange="">
				                				<option value=0>Evento en Desarrollo</option>
				                				<option value=1>Evento Anunciado</option>
				                			</select>
				                		</td>
				                	</tr>
				                	<tr>
				                		<td>Fecha de la Notificación</td>
				                		<td><input class="form-control" type="date" id="fecha" name="fecha" value="<?php echo date('Y-m-d') ?>" readonly></td>
				                	</tr>
				                	<tr>
				                		<td>Medio:</td>
				                		<td><select class="form-control single" name="medio" id="medio" onchange="fmedio()" placeholder="Seleccione un elemento">				                				
				                				<option value=1>Television</option>
				                				<option value=2>Escrito/Digital</option>
				                				<option value=3>Red Social</option>
				                				<option value=4>Radio</option>
				                				<option value=5>Información Extraoficial</option>
				                			</select>

				                			<div id="dmedio">
				                				
				                			</div>
				                		</td>
				                	</tr>
				                </table>
				                <h4>Fuente (4)</h4>
				                <table class="table">
				                	<tr>
				                		<td>Procedencia:</td>
				                		<td><select  class="form-control single" name="fuentep" id="fuentep" onchange="">
				                				<option value=0>Gobierno</option>
				                				<option value=1>Otras Fuentes Oficiales del Estado</option>
				                				<option value=2>Cuerpo Dip, Org. Int, ONGs</option>
				                				<option value=3>Empresa Privada</option>
				                				<option value=4>Sociedad Civil</option>
				                				<!-- <option value=5></option>
				                				<option value=6></option> -->
				                			</select>
				                		</td>
				                	</tr>
				                	<tr>
				                		<td>Valoración:</td>
				                		<td><select  class="form-control single" name="fuentev" id="fuentev" onchange="">
				                				<option value=1>Muy Confiable</option>
				                				<option value=2>Confiable</option>
				                				<option value=3>Medianamente Confiable</option>
				                				<option value=4>Poco Confiable</option>
				                				<option value=5>Desconfiable</option>
				                				<option value=6>Nunca se trabajo con la fuente</option>
				                			</select>
				                		</td>
				                	</tr>
				                </table>
				                <h4>Información (5)</h4>
				                <table class="table">
				                	<tr>
				                		<td>Credibilidad: <i class="fa fa-help"></i></td>
				                		<td><select  class="form-control single" name="infoc" id="infoc" onchange="">
				                				<option value=1>Muy Creible</option>
				                				<option value=2>Creible</option>
				                				<option value=3>Medianamente Creible</option>
				                				<option value=4>Poco Creible</option>
				                				<option value=5>No Creible</option>
				                				<option value=6>Nunca se recibio este tipo de información</option>
				                			</select>
				                		</td>
				                	</tr>
				                	<tr>
				                		<td>Pertinencia: <i class="fa fa-help"></i></td>
				                		<td><select  class="form-control single" name="infop" id="infop" onchange="">
				                				<option value=1>Muy Pertinente</option>
				                				<option value=2>Pertinente</option>
				                				<option value=3>Medianamente Pertinente</option>
				                				<option value=4>Poco Pertinente</option>
				                				<option value=5>No Pertinente</option>
				                				<option value=6>Se desconoce la Pertinencia</option>
				                			</select>
				                		</td>
				                	</tr>
				                </tABLE>
				                <h4>Actores (8)</h4>
				                <table class="table">
				                	<tr><th>Generadores del Conflicto / Parte Demandada</th></tr>
				                	<tr><td><label><input  name="act0[]" id="" value=1  type="checkbox" > Gobierno Central</label></td></tr>
				                	<tr><td><label><input  name="act0[]" id="regional" value=2 type="checkbox" onchange="mostrar('dregional0')" > Gobierno Regional</label>
				                		<div class="hide" id="dregional0">
				                			<select  class="form-control single" name="regional0" id="regional0" >
				                				<option value=1>La Paz</option>
				                				<option value=2>Santa Cruz</option>
				                				<option value=3>Cochabamba</option>
				                				<option value=4>Oruro</option>
				                				<option value=5>Tarija</option>
				                				<option value=6>Sucre</option>
				                				<option value=7>Beni</option>
				                				<option value=8>Potosi</option>
				                				<option value=9>Pando</option>
				                			</select>
				                		</div>
				                	</td></tr>
				                	<tr><td><label><input  name="act0[]" id="" value=3 type="checkbox" onchange="mostrar('dmunicipal0')" > Gobierno Municipal</label>
				                		<div class="hide" id="dmunicipal0">
				                			<input class="form-control" name="municipal0" placeholder="Ingrese la Alcaldia" >
				                		</div>
				                	</td></tr>
				                	<tr><td><label><input  name="act0[]" id="" value=4 type="checkbox" onchange="mostrar('dempresa0')"> Empresa Privada</label>
				                		<div class="hide" id="dempresa0">
				                			<input class="form-control" name="empresa0" placeholder="Ingrese la Empresa" >
				                		</div>
				                	</td></tr>
				                	<tr><td><label><input  name="act0[]" id="scivil" value=5 type="checkbox" onchange="mostrar('dscivil0')" > Sociedad Civil Organizada</label>
				                		<div class="hide" id="dscivil0">
				                			<select  class="form-control single" name="scivil0" id="scivil0" >
				                				<option value=1>Sector Salud</option>
				                				<option value=2>Sector Educacion</option>
				                				<option value=3>Gremiales</option>
				                				<option value=4>Sector Transporte</option>
				                				<option value=5>Juntas Vecinales</option>
				                				<option value=6>Mineros</option>
				                			</select>
				                		</div>
				                	</td></tr>
				                	<tr><td><label><input  name="act0[]" id="" value=6 type="checkbox" > Sociedad Civil No Organizada</label></td></tr>
				                	<tr><td><label><input  name="act0[]" id="persona0" value=7 type="checkbox" onchange="mostrar('dpersona0')" > Persona Natural</label>
				                		<div class="hide" id="dpersona0">
				                			<input class="form-control" name="persona0" placeholder="Ingrese el nombre de la persona" >
				                		</div>
				                	</td></tr>

				                	<tr><th>Sector Afectado / Parte Demandante</th></tr>
				                	<tr><td><label><input  name="act1[]" id="central1" value=1 type="checkbox" > Gobierno Central</label></td></tr>
				                	<tr><td><label><input  name="act1[]" id="regional1" value=2 type="checkbox" onchange="mostrar('dregional1')" > Gobierno Regional</label>
				                	<div class="hide" id="dregional1">
				                			<select  class="form-control single" name="regional1" id="regional1" >
				                				<option value=1>La Paz</option>
				                				<option value=2>Santa Cruz</option>
				                				<option value=3>Cochabamba</option>
				                				<option value=4>Oruro</option>
				                				<option value=5>Tarija</option>
				                				<option value=6>Sucre</option>
				                				<option value=7>Beni</option>
				                				<option value=8>Potosi</option>
				                				<option value=9>Pando</option>
				                			</select>
				                		</div>
				                	</td></tr>
				                	<tr><td><label><input  name="act1[]" id="municipal1" value=3 type="checkbox" onchange="mostrar('dmunicipal1')" > Gobierno Municipal</label>
				                	<div class="hide" id="dmunicipal1">
				                			<input class="form-control" name="municipal1" placeholder="Ingrese la Alcaldia" >
				                		</div>
				                	</td></tr>
				                	<tr><td><label><input  name="act1[]" id="empresa1" value=4 type="checkbox" onchange="mostrar('dempresa1')"> Empresa Privada</label>
				                	<div class="hide" id="dempresa1">
				                			<input class="form-control" name="empresa1" placeholder="Ingrese la Empresa" >
				                		</div>
				                	</td></tr>
				                	<tr><td><label><input  name="act1[]" id="scivil1" value=5 type="checkbox" onchange="mostrar('dscivil1')"> Sociedad Civil Organizada</label>
				                	<div class="hide" id="dscivil1">
				                			<select  class="form-control single" name="scivil1" id="scivil1" >
				                				<option value=1>Sector Salud</option>
				                				<option value=2>Sector Educacion</option>
				                				<option value=3>Gremiales</option>
				                				<option value=4>Sector Transporte</option>
				                				<option value=5>Juntas Vecinales</option>
				                				<option value=6>Mineros</option>
				                			</select>
				                		</div>
				                	</td></tr>
				                	<tr><td><label><input  name="act1[]" id="ncivil1" value=6 type="checkbox" > Sociedad Civil No Organizada</label></td></tr>
				                	<tr><td><label><input  name="act1[]" id="persona1" value=7 type="checkbox" onchange="mostrar('dpersona1')"> Persona Natural</label>
				                	<div class="hide" id="dpersona1">
				                			<input class="form-control" name="persona1" placeholder="Ingrese el nombre de la persona" >
				                		</div>
				                	</td></tr>
				                </table>
				                <table class="table">
				                	<tr>
				                		<th>Tipo de Medida: </th>
				                		<td><select  class="form-control single" name="medida" id="medida" onchange="fmedida()">
				                				<option value=0>Ninguno</option>
				                				<option value=1>Manifestación Social</option>
				                				<option value=2>Paro de Transporte</option>
				                				<option value=3>Toma de Instalaciones</option>
				                				<option value=4>Huelga</option>
				                				<option value=5>Paro Civico</option>
				                				<option value=6>Otro</option>
				                			</select>
				                			<div class="dmedida"></div>
				                		</td>
				                	</tr>
				                </tABLE>
				                <h4>Noticia (9)</h4>
				                <table class="table">
				                	<tr>
				                		<td width="30%">Valoración de Riesgo: </td>
				                		<td width="70%"><select  class="form-control single" name="riesgo" id="riesgo" onchange="">
				                				<option value=1>Muy Bajo</option>
				                				<option value=2>Bajo</option>
				                				<option value=3>Medio</option>
				                				<option value=4>Alto</option>
				                				<option value=5>Muy Alto</option>
				                			</select></td>
				                	</tr>
				                	<tr>
				                		<td>Resumen de la Noticia: </td>
				                		<td><textarea class="form-control"  readonly id="resumen" name="resumen"></textarea></td>
				                	</tr>
				                	<tr>
				                		<td>Fecha de la Noticia: </td>
				                		<td><input type="date" class="form-control" name="nfecha" id="nfecha" value="<?php echo date('Y-m-d') ?>"></td>
				                	</tr>
				                	<tr>
				                		<td>Lugar de la Noticia: </td>
				                		<td><input class="form-control" name="lugar" id="lugar" placeholder="Ingrese la dirección del evento" onchange="flugar()"></td>
				                	</tr>
				                	<tr>
				                		<td colspan=2>Ubicación en el Mapa: <br><br>
				                			<img class="img-thumbnail img-fluid" src="<?php echo base_url('img/mapa.png') ?>">
				                		</td>
				                	</tr>
				                	<tr>
				                		<td>Descripción del hecho: </td>
				                		<td><textarea class="form-control" id="hecho" name="hecho"></textarea></td>
				                	</tr>
				                	<tr>
				                		<td>Enlace de Referencia: </td>
				                		<td><input class="form-control" id="link" name="link" ></td>
				                	</tr>
				                </table>
				                <h4>Probabilidad e Impacto</h4>
				                <table class="table">
				                	<tr>
				                		<th>Probabilidad de que el evento se repita:</th>
				                		<td>
				                			<select  class="form-control single" name="prob" id="prob" onchange="">
				                				<option value=5>Muy Alta (80% - 100%)</option>
				                				<option value=4>Alta (60% - 80%)</option>
				                				<option value=3>Media (40% - 60%)</option>
				                				<option value=2>Poca (20% - 40%)</option>
				                				<option value=1>Ninguna (0% - 20%)</option>
				                			</select>
				                		</td>
				                	</tr>
				                	<tr>
				                		<th>Impacto Personal:</th>
				                		<td>
				                			<label><input  name="heridos" id="heridos"  type="checkbox" > Heridos</label><br>
				                			<label><input  name="muertos" id="muertos"  type="checkbox" > Muertos</label>
				                		</td>
				                	</tr>
				                	<tr>
				                		<th>Impacto en Bienes o Instalaciones:</th>
				                		<td>
				                			<label><input  name="vehiculos" id="vehiculos"  type="checkbox" > Vehiculos</label><br>
				                			<label><input  name="instalaciones" id="instalaciones"  type="checkbox" > Instalaciones</label>
				                		</td>
				                	</tr>
				                	<tr>
				                		<th>Impacto Operacional:</th>
				                		<td>
				                			<label><input  name="io1" id="cierre"  type="checkbox" > Cierre de Oficianas</label><br>
				                			<label><input  name="io2" id="intran"  type="checkbox" > Intransitabilidad Urbana</label><br>
				                			<label><input  name="io3" id="intran"  type="checkbox" > Bloqueo de Carreteras</label><br>
				                			<!-- <label><input  name="io4" id="intran"  type="checkbox" > Intransitabilidad Urbana</label> -->
				                		</td>
				                	</tr>
				                </table>
				                <p class="center">
				                	<button class="btn btn-primary btn-lg " id="enviar" disabled >Enviar <i class="fa fa-send"></i></button>
				                </p>
				            </form>
				        </div>
				    </div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</body>

<script type="text/javascript">

	//amenazas
	var peligros = new Array();
	peligros[1]='Accidente de Transito';
	peligros[2]='Inundación';
	peligros[3]='Incendio';
	peligros[4]='Desastre Natural';
	peligros[5]='Accidente Aereo';
	//conflictos
	var social=new Array();
	social[1]="Acceso al Trabajo";
	social[2]="Acceso a la Salud";
	social[3]="Acceso a la Educación";
	social[4]="Movilidad Social";
	social[5]="Asuntos Economicos";
	var cultural=Array();
	cultural[21]="Ideologico Político";
	cultural[22]="Seguridad Ciudadana";
	cultural[23]="Asuntos Etnicos";
	cultural[24]="Asuntos Religioso Culturales";
	cultural[25]="Asuntos de Genero";
	cultural[26]="Asuntos Socioambientales";
	var estructural=new Array();
	estructural[11]="Leyes";
	estructural[12]="Politicas";
	estructural[13]="Procedimientos";

	//crimenes
	var crimendc=new Array();
	crimendc[1]="Robo a Transeunte";
	crimendc[2]="Agresion Sexual";
	var crimeno=new Array();
	crimeno[11]="Trata y Trafico de Personas";
	crimeno[12]="Robo de Domicilios";
	crimeno[13]="Robo de Vehiculos";
	crimeno[14]="Robo de Autopartes";
	crimeno[15]="Narcotrafico";
	crimeno[16]="Asaltos y Atracos";
	//otros
	var otros=new Array();
	otros[1]="Daño Reputacional";
	otros[2]="Desaparición de Personas";

	var medios=new Array();
	//medios de tv
	medios[1]=new Array();
	medios[1][1]="Red Uno";
	medios[1][2]="ATB";
	medios[1][3]="Unitel";
	medios[1][4]="PAT";
	//medios escritos
	medios[2]=new Array();
	medios[2][1]="El Diario";
	medios[2][2]="La Razon";
	medios[2][3]="El Extra";
	//red social
	medios[3]=new Array();
	medios[3][1]="Facebook";
	medios[3][2]="Twitter";
	medios[3][3]="Watsapp";
	//Radio
	medios[4]=new Array();
	medios[4][1]="Panamericana";
	medios[4][2]="Fides";

	function fmedio() {
		med=document.getElementById('medio').value;
		var info="";
		switch(med){
			case '1': //television
			case '2'://Escrito/Digital
			case '3'://Red Social
			case '4'://Radio
				info='<br><select class="form-control" name="emedio" >';
				for (var i = 1; i < medios[med].length; i++) {
					info=info+'<option value="'+i+'">'+medios[med][i]+'</option>';
				};
				info=info+'</select>';
				break;
			case '5'://Información Extraoficial
				break;
			case '6'://otro
				break;
		}
		document.getElementById('dmedio').innerHTML=info;
	}
	
	fmedio();
	// famenaza();
	//funcion para mostrar un div
	function mostrar(idiv) {

		cdiv=document.getElementById(idiv).className;
		if(cdiv=="hide"){
			document.getElementById(idiv).className="";
		}else{
			document.getElementById(idiv).className="hide";
		}
	}
	//amenaza
	function dame () {
		var info="";
		amz=document.getElementById('amenaza').value;
		document.getElementById('enviar').disabled=false;
		cod='0001';
		ncod="";
		switch(amz){
			case '0':
				document.getElementById('enviar').disabled=true;
				break;
			case '1':
				ncod='T-'+cod;
				break;
			case '2':
				ncod='CA-'+cod;
				break;
			case '3':
				ncod='Cr-'+cod;
				info='<br><select class="form-control" name="crimen" >'+
				'<optgroup label="Delincuencia Comun">';
				for (var i = 1; i < crimendc.length; i++) {
					info=info+'<option value="'+i+'">'+crimendc[i]+'</option>';
				};
				info=info+'</optgroup><optgroup label="Crimen Organizado">';
				for (var i = 11; i < crimeno.length; i++) {
					info=info+'<option value="'+i+'">'+crimeno[i]+'</option>';
				};
				info=info+'</optgroup>/select>';
				break;
			case '4':
				ncod='Cs-'+cod;
				info='<br><select class="form-control" name="social" >'+
				'<optgroup label="Reproducción Social">';
				for (var i = 1; i < social.length; i++) {
					info=info+'<option value="'+i+'">'+social[i]+'</option>';
				};
				info=info+'</optgroup><optgroup label="Conflicto Estructural">';
				for (var i = 11; i < estructural.length; i++) {
					info=info+'<option value="'+i+'">'+estructural[i]+'</option>';
				};
				info=info+'</optgroup><optgroup label="Conflicto Cultural">';
				for (var i = 21; i < cultural.length; i++) {
					info=info+'<option value="'+i+'">'+cultural[i]+'</option>';
				};
				info=info+'</optgroup></select>';
				break;
			case '5':
				ncod='P-'+cod;
				info='<br><select class="form-control" name="peligro" >';
				for (var i = 1; i < peligros.length; i++) {
					info=info+'<option value="'+i+'">'+peligros[i]+'</option>';
				};
				info=info+'</select>';
				break;
			case '6':
				ncod='O-'+cod;
				var info='<br><select class="form-control" id="otraa" name="otro" onchange="aotro()" >';
				for (var i = 1; i < otros.length; i++) {
					info=info+'<option value="'+i+'">'+otros[i]+'</option>';
				};
				info=info+'<option value=0 >Otro</option>';
				info=info+'</select>';
				break;
		}
		document.getElementById('damenaza').innerHTML=info;
		document.getElementById('codigo').value=ncod;
	}
	function aotro() {
		otra=document.getElementById('otraa').value;
		if(otra==0){
			document.getElementById('dotraa').className='';
		}else{
			document.getElementById('dotraa').className='hide';
		}

	}

</script>