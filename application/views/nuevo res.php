<!-- para el mapa -->
<?php $itipo="" ?>
<script type="text/javascript">

	var codigos = new Array();
	var ults = new Array();
	var abrs = new Array();
	var tamz = new Array();
	var samz = new Array();
	var gamz = new Array();

	var emed=new Array();
	var tmed=new Array();
	
	<?php foreach ($medida as $value) { 
		if($value['tipo']>1){ ?>
		emed[<?php echo $value['id'] ?>]="<?php echo $value['extra'] ?>"; <?php } ?>
		tmed[<?php echo $value['id'] ?>]=<?php echo $value['tipo'] ?>;
	<?php } ?>
	var orden=new Array();
	var medios=new Array();
	var lugares=new Array();
	var om=0;am=0;
	<?php foreach ($tmedio as $value) { ?>
		//por tipo
		medios[<?= $value['id'] ?>]=new Array();
		<?php foreach ($medio[$value['id']] as $valor) { ?>
			orden[om]=<?= $valor['id'] ?>;
			medios[<?= $value['id'] ?>][om]="<?= $valor['nombre'] ?>";
			lugares[om]="<?= $valor['lugar'] ?>";
			om++;
		<?php }
	} 

	foreach ($amenaza as $value) { ?>
		codigos[<?= $value['id'] ?>]=<?= $value['last'] ?>;
		abrs[<?= $value['id'] ?>]="<?= $value['cod'] ?>";
		tamz[<?= $value['id'] ?>]="<?= $value['tipo'] ?>";
		<?php if($value['tipo']==2){ ?>
			samz[<?= $value['id'] ?>]=new Array();
			<?php foreach ($samenaza[$value['id']] as $key) { ?>
				ults[am]=<?= $key['id'] ?>;
				samz[<?= $value['id'] ?>][am]="<?= $key['nombre'] ?>";
				gamz[am]="<?= $key['tipo'] ?>";
				am++;
			<?php } 
			} ?>
	<?php } ?>

</script>
<body>
	<div class="container">
		<div class="row">

			<div class="col-md-2"></div>
			<div class="col-md-8">
				<br>
				<div id="page-title " >
					<div class="row">
						<div class="col-md-10">
						    <h2>Nueva Información</h2>
						    <p>Formulario de información de seguridad.</p>
					    </div>
					    <div class="col-md-2 right">
					    	<button class="btn btn-info right btn-sm" onclick="inicio()" title="Regresar"><b><<</b></button>
					    </div>
				    </div>
				</div>
				<div class="box">
				    <div class="box-body">
				        <h3 class="title-hero">
				            Datos
				        </h3>
				        <div class="example-box-wrapper">
				            <?php echo validation_errors();
                    				echo form_open_multipart('/inicio/nuevo'); ?>
				                <!-- aqui viene el formulario -->
				                <table class="table">
				                	<tr>
				                		<td width="30%">Nombre:</td>
				                		<td width="70%"><input class="form-control" name="nombre" id="nombre" placeholder="Ingrese el nombre de la información" required></td>
				                	</tr>
				                	<tr>
				                		<td>Amenaza: </td>
				                		<td><select  class="form-control single" name="amenaza" id="amenaza" onchange="famenaza()" required>
				                				<option value=0>...Indique el tipo de Amenaza</option>
				                				<option value=1>Terrorismo <!-- (T) --></option>
				                				<option value=2>Conflicto Armado<!-- (CA) --></option>
				                				<option value=3>Crimen <!-- (Cr) --></option>
				                				<option value=4>Conflictividad Social <!-- (CS) --></option>
				                				<option value=5>Peligros <!-- (P) --></option>
				                				<option value=100>Otros <!-- (O) --></option>
				                			</select>

				                			<div id="damenaza" >
				                				<!-- Hola -->
				                			</div>
				                			<div class="hide" id="dotraa"><br>
					                			<input class="form-control" name="otra" placeholder="Ingrese la Amenaza" >
					                		</div>
				                		</td>
				                	</tr>
				                	<tr>
				                		<td>Código:</td>
				                		<td><input class="form-control" name="codigo" id="codigo" readonly></td>
				                	</tr>
				                	<tr>
				                		<td>Estado:</td>
				                		<td><select  class="form-control single" name="estado" id="estado" onchange="">
				                				<option value=0>Evento en Desarrollo</option>
				                				<option value=1>Evento Anunciado</option>
				                				<option value=2>Noticia Relacionada</option>
				                			</select>
				                		</td>
				                	</tr>
				                	<tr>
				                		<td>Fecha del Registro</td>
				                		<td><input class="form-control" type="date" id="fecha" name="fecha" value="<?php echo date('Y-m-d') ?>" readonly></td>
				                	</tr>
				                	<tr>
				                		<td>Medio:</td>
				                		<td><select class="form-control single" name="tmedio" id="medio" onchange="fmedio()" placeholder="Seleccione un elemento">				                				
				                			<?php foreach ($tmedio as $value) {  ?>
				                				<option value=<?= $value['id'] ?>><?= $value['nombre'] ?></option>
				                			<?php } ?>
				                				<option value=0>Información Extraoficial</option>
				                			</select>

				                			<div id="dmedio">
				                				
				                			</div>
				                		</td>
				                	</tr>
				                </table>
				                <h4>Fuente</h4>
				                <table class="table">
				                	<tr>
				                		<td>Procedencia:</td>
				                		<td><select  class="form-control single" name="fuentep" id="fuentep" onchange="">
				                			<?php foreach ($fuente as $value) { ?>
				                				<option value=<?= $value['id'] ?>><?= $value['nombre'] ?></option>
				                			<?php } ?>
				                			</select>
				                		</td>
				                	</tr>
				                	<tr>
				                		<td>Valoración:</td>
				                		<td><select  class="form-control single" name="fuentev" id="fuentev" onchange="">
				                				<option value=1>Muy Confiable</option>
				                				<option value=2>Confiable</option>
				                				<option value=3>Medianamente Confiable</option>
				                				<option value=4>Poco Confiable</option>
				                				<option value=5>Desconfiable</option>
				                				<option value=6>Nunca se trabajo con la fuente</option>
				                			</select>
				                		</td>
				                	</tr>
				                </table>
				                <h4>Información</h4>
				                <table class="table">
				                	<tr>
				                		<td>Credibilidad: <i class="fa fa-help"></i></td>
				                		<td><select  class="form-control single" name="infoc" id="infoc" onchange="">
				                				<option value=1>Muy Creible</option>
				                				<option value=2>Creible</option>
				                				<option value=3>Medianamente Creible</option>
				                				<option value=4>Poco Creible</option>
				                				<option value=5>No Creible</option>
				                				<option value=6>Nunca se recibio este tipo de información</option>
				                			</select>
				                		</td>
				                	</tr>
				                	<tr>
				                		<td>Pertinencia: <i class="fa fa-help"></i></td>
				                		<td><select  class="form-control single" name="infop" id="infop" onchange="">
				                				<option value=1>Muy Pertinente</option>
				                				<option value=2>Pertinente</option>
				                				<option value=3>Medianamente Pertinente</option>
				                				<option value=4>Poco Pertinente</option>
				                				<option value=5>No Pertinente</option>
				                				<option value=6>Se desconoce la Pertinencia</option>
				                			</select>
				                		</td>
				                	</tr>
				                </tABLE>
				                <div id="actores" class="hide">
				                <h4>Actores</h4>
				                <table class="table table-bordered">
				                	<tr>
				                		<th>Generadores del Conflicto / Parte Demandada</th>
				                		<th>Sector Afectado / Parte Demandante</th>
				                	</tr>
				                	<tr><td><label><input  name="act0[]" id="" value=1 type="checkbox" > Gobierno Central</label></td>
				                		<td><label><input  name="act1[]" id="" value=1 type="checkbox" > Gobierno Central</label></td></tr>
				                	<tr><td><label><input  name="act0[]" id="regional" value=2 type="checkbox" onchange="mostrar('dregional0')" > Gobierno Regional</label>
				                		<div class="hide" id="dregional0">
				                			<select  class="form-control single" name="parte_2_0" id="regional0" >
				                				<?php foreach ($depr as $value) { ?>
				                					<option value=<?= $value['id'] ?>><?= $value['nombre'] ?></option>
				                				<?php } ?>
				                			</select>
				                		</div></td>
				                		<td><label><input  name="act1[]" id="regional1" value=2 type="checkbox" onchange="mostrar('dregional1')" > Gobierno Regional</label>
				                		<div class="hide" id="dregional1">
				                			<select  class="form-control single" name="parte_2_1" id="regional1" >
				                				<?php foreach ($depr as $value) { ?>
				                					<option value=<?= $value['id'] ?>><?= $value['nombre'] ?></option>
				                				<?php } ?>
				                			</select>
				                		</div>
				                		</td>
				                	</tr>
				                	<tr><td><label><input  name="act0[]" id="" value=3 type="checkbox" onchange="mostrar('dmunicipal0')" > Gobierno Municipal</label>
				                		<div class="hide" id="dmunicipal0">
				                			<input class="form-control" name="parte_3_0" placeholder="Ingrese la Alcaldia" >
				                		</div>
				                	</td>
				                	<td><label><input  name="act1[]" id="municipal1" value=3 type="checkbox" onchange="mostrar('dmunicipal1')" > Gobierno Municipal</label>
				                	<div class="hide" id="dmunicipal1">
				                			<input class="form-control" name="parte_3_1" placeholder="Ingrese la Alcaldia" >
				                		</div>
				                	</td>
				                	</tr>
				                	<tr><td><label><input  name="act0[]" id="" value=4 type="checkbox" onchange="mostrar('dempresa0')"> Empresa Privada</label>
				                		<div class="hide" id="dempresa0">
				                			<input class="form-control" name="parte_4_0" placeholder="Ingrese la Empresa" >
				                		</div>
				                	</td>
				                	<td><label><input  name="act1[]" id="empresa1" value=4 type="checkbox" onchange="mostrar('dempresa1')"> Empresa Privada</label>
				                	<div class="hide" id="dempresa1">
				                			<input class="form-control" name="parte_4_1" placeholder="Ingrese la Empresa" >
				                		</div>
				                	</td>
				                	</tr>
				                	<tr><td><label><input  name="act0[]" id="scivil" value=5 type="checkbox" onchange="mostrar('dscivil0')" > Sociedad Civil Organizada</label>
				                		<div class="hide" id="dscivil0">
				                			<select  class="form-control single" name="parte_5_0" id="scivil0" >
				                				<?php foreach ($sector as $value) { ?>
				                					<option value=<?= $value['id'] ?>><?= $value['nombre'] ?></option>
				                				<?php } ?>
				                			</select>
				                		</div>
				                	</td>
				                	<td><label><input  name="act1[]" id="scivil1" value=5 type="checkbox" onchange="mostrar('dscivil1')"> Sociedad Civil Organizada</label>
				                	<div class="hide" id="dscivil1">
				                			<select  class="form-control single" name="parte_5_1" id="scivil1" >
				                				<?php foreach ($sector as $value) { ?>
				                					<option value=<?= $value['id'] ?>><?= $value['nombre'] ?></option>
				                				<?php } ?>
				                			</select>
				                		</div>
				                	</td>
				                	</tr>
				                	<tr><td><label><input  name="act0[]" id="" value=6 type="checkbox" > Sociedad Civil No Organizada</label></td>
				                	<td><label><input  name="act1[]" id="ncivil1" value=6 type="checkbox" > Sociedad Civil No Organizada</label></td></tr>
				                	<tr><td><label><input  name="act0[]" id="persona0" value=7 type="checkbox" onchange="mostrar('dpersona0')" > Persona Natural</label>
				                		<div class="hide" id="dpersona0">
				                			<input class="form-control" name="parte_7_0" placeholder="Ingrese el nombre de la persona" >
				                		</div>
				                	</td>
				                	<td><label><input  name="act1[]" id="persona1" value=7 type="checkbox" onchange="mostrar('dpersona1')"> Persona Natural</label>
				                	<div class="hide" id="dpersona1">
				                			<input class="form-control" name="parte_7_1" placeholder="Ingrese el nombre de la persona" >
				                		</div>
				                	</td></tr>
				                </table>
				                <table class="table">
				                	<tr>
				                		<th>Tipo de Medida: </th>
				                		<td><select  class="form-control single" name="medida" id="medida" onchange="fmedida()">
				                				<option value=0>Ninguno</option>
				                				<?php foreach ($medida as $value) { ?>
				                					<option value=<?php echo $value['id'] ?>><?php echo $value['nombre'] ?></option>
				                				<?php } ?>
				                			</select>
				                			<div id="dmedida" class="hide">
					                				<div class="col-md-4" id="med1">
					                					<br>
					                					<input name="emedida" id="emedida" placeholder="Número de Personas" class="form-control" >
					                				</div>
					                				<div class="col-md-4" id="med2">
					                					<br>
					                					<b><font color="#fff"><a class="btn btn-danger btn-sm" onclick="susnmed()">- Menos</a><a class="btn btn-primary btn-sm" onclick="addnmed()">&nbsp+&nbspMas&nbsp&nbsp</a></font></b>
					                				</div>
					                				<div class="col-md-2" >
					                					<br>
					                					<label>Inicio: </label>
					                				</div>
					                				<div class="col-md-4" id="med3"><br>
					                					<!-- <input type="time" class="form-control"> -->
					                					<select class="form-control" name="inicio">
					                						<?php for ($i=0; $i < 24; $i++) {   ?>
					                						<option><?php echo sprintf("%'.02d", $i); ?>:00</option>
					                						<?php } ?>
					                					</select>
					                				</div>
					                				<div class="col-md-2" >
					                					<br>
					                					<label>Fin: </label>
					                				</div>
					                				<div class="col-md-4" id="med3"><br>
					                					<!-- <input type="time" class="form-control"> -->
					                					<select class="form-control" name="fin">
					                						<?php for ($i=0; $i < 24; $i++) {   ?>
					                						<option><?php echo sprintf("%'.02d", $i); ?>:00</option>
					                						<?php } ?>
					                					</select>
					                				</div>
				                				<div class="col-md-4" >
				                					<br>
				                					Ejecutado por: 
				                				</div>
				                				<div class="col-md-8" id="med2"><br>
				                					<select  class="form-control single" name="ejecutor" id="scivil1" >
				                						<option value=0>Parte Demandante/Sector Afectado</option>
						                				<?php foreach ($sector as $value) { ?>
						                					<option value=<?= $value['id'] ?>><?= $value['nombre'] ?></option>
						                				<?php } ?>
						                			</select>
				                				</div>
				                			</div>
				                		</td>
				                	</tr>
				                </table>
				                </div>
				                <h4>Noticia</h4>
				                <table class="table">
				                	<!-- <tr>
				                		<td>Resumen de la Noticia: </td>
				                		<td><textarea class="form-control"  readonly id="resumen" name="resumen"></textarea></td>
				                	</tr> -->
				                	<tr>
				                		<td>Fecha de la Noticia: </td>
				                		<td><input type="date" class="form-control" name="nfecha" id="nfecha" value="<?php echo date('Y-m-d') ?>"></td>
				                	</tr>
				                	<tr>
				                		<td>Lugar de la Noticia: <br><br><a class="btn btn-info" onclick="abrirVentana('<?php echo base_url('inicio/geocodificacion/') ?>');" style="color: #fff; "><b>Buscar en el mapa</b></a></td>
				                		<td><input class="form-control" name="lugar" id="lugar" placeholder="Ingrese la dirección del evento" onchange="flugar()" required><br>
				                			<input class="form-control" name="coordenadas" id="punto" value="0,0" readonly></td>
				                	</tr>
				                	<tr>
				                		<td>Imagen de la noticia: <br><br></td>
				                		<td><input type="file" class="form-control" name="img"  accept="image/*" multiple data-show-upload="true" data-show-caption="true" data-show-preview="true"></td>
				                			<!-- <img class="img-thumbnail img-fluid img-responsive" style="max-width: 100%; and height: auto;" src="<?php echo base_url('img/noticias/0.png') ?>">  -->
				                	</tr> 
				                	<tr>
				                		<td>Comentario: </td>
				                		<td><textarea class="form-control" id="hecho" name="hecho" placeholder="Adicionar comentario"></textarea></td>
				                	</tr>
				                	<tr>
				                		<td>Enlace de Referencia: </td>
				                		<td><input class="form-control" id="link" name="link" ></td>
				                	</tr>
				                </table>
				                <h4>Probabilidad e Impacto</h4>
				                <table class="table">
				                	<tr>
				                		<th>Probabilidad de que<br>el evento se repita:</th>
				                		<td width="70%">
				                			<select  class="form-control single" name="prob" id="prob" onchange="">
				                				<option value=5>Muy Alta (80% - 100%)</option>
				                				<option value=4>Alta (60% - 80%)</option>
				                				<option value=3>Media (40% - 60%)</option>
				                				<option value=2>Poca (20% - 40%)</option>
				                				<option value=1>Ninguna (0% - 20%)</option>
				                			</select>
				                		</td>
				                	</tr>
				                	<?php foreach ($imp as $value) { 
				                		if($value['tipo']!=$itipo){ 
				                			$itipo=$value['tipo']; ?>
				                		</td>
				                	</tr>
				                	<tr>
				                		<th>Impacto <?= $value['tipo'] ?>:</th>
				                		<td>
				                	<?php } ?> 
				                		<label><input name="check[]" value="<?= $value['id'] ?>"  onchange="mostrar('dimp_<?= $value['id'] ?>')" type="checkbox" > <?= ucfirst($value['nombre']) ?></label><br>
				                		<?php if($value['es']==1){ ?>
				                		<div class="hide" id="dimp_<?= $value['id'] ?>">
				                			<input type="number" min=1 class="form-control" name="imp_<?= $value['id'] ?>" value=1 placeholder="Ingrese el número">
				                			<br><textarea class="form-control" placeholder="Ingrese los nombres de los <?= $value['nombre'] ?>" name="det_<?= $value['id'] ?>"></textarea><br>
				                		</div>
				                	<?php } } ?>
				                	</tr>
				                	<tr>
				                		<td width="30%">Valoración de Riesgo: </td>
				                		<td width="70%"><select  class="form-control single" name="riesgo" id="riesgo" onchange="">
				                				<option value=1>Muy Bajo</option>
				                				<option value=2>Bajo</option>
				                				<option value=3>Medio</option>
				                				<option value=4>Alto</option>
				                				<option value=5>Muy Alto</option>
				                			</select></td>
				                	</tr>
				                </table>
				                <p class="center">
				                	<button class="btn btn-primary btn-lg " id="enviar" disabled >Enviar <i class="fa fa-send"></i></button>
				                	<a class="btn btn-lg btn-default" onclick="inicio()">Cancelar</a>
				                </p>
				            </form>
				        </div>
				    </div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</body>

<script type="text/javascript">

	var cmed=1;
	nmeds=new Array();
	nmeds[1]='menos de 50 personas';
	nmeds[2]='50 - 100 personas';
	nmeds[3]='100 - 500 personas';
	nmeds[4]='500 - 1000 personas';
	nmeds[5]='más de 1000 personas';
	
	function fmedio() {
		med=document.getElementById('medio').value;
		var info=""; var llugar="";
		if(med!=0){
			info='<br><select class="form-control" name="medio" >';
			for (var i = 0; i < medios[med].length; i++) {
				if(medios[med][i]!=undefined){
					if(lugares[i]!=''&&lugares[i]!=llugar)
						info=info+'</optgroup><optgroup label="'+lugares[i]+'">';
					llugar=lugares[i];
					info=info+'<option value="'+orden[i]+'">'+medios[med][i]+'</option>';
				}
			};
			info=info+'</optgroup></select>';
		}
		document.getElementById('dmedio').innerHTML=info;
	}
	
	fmedio();
	//funcion para mostrar un div
	function mostrar(idiv) {

		cdiv=document.getElementById(idiv).className;
		if(cdiv=="hide"){
			document.getElementById(idiv).className="";
		}else{
			document.getElementById(idiv).className="hide";
		}
	}
	//amenaza
	function famenaza() {
		var info="";
		var lgroup="";
		amz=document.getElementById('amenaza').value;
		document.getElementById('enviar').disabled=false;
		document.getElementById('dotraa').className='hide';
		if(amz==0)
			document.getElementById('enviar').disabled=true;
		else{
			document.getElementById('codigo').value=abrs[amz]+'-'+codigos[amz];
			if(tamz[amz]==2){
				info='<br><select class="form-control" name="tamenaza" id="tamenaza" onchange="aotro()">';
				for (var i = 0; i < samz[amz].length; i++) {
					if(samz[amz][i]!=undefined){
						if(gamz[i]!=''&&gamz[i]!=lgroup)
							info=info+'</optgroup><optgroup label="'+gamz[i]+'">';
						lgroup=gamz[i];
						info=info+'<option value="'+ults[i]+'">'+samz[amz][i]+'</option>';
					}
				};
				if(amz==100)
					info=info+'<option value=1000>Otro</option>';
				info=info+'</optgroup></select>';
			}
		}
		if(amz==4)
			document.getElementById('actores').className="";
		else
			document.getElementById('actores').className="hide";
		document.getElementById('damenaza').innerHTML=info;
	}
	function fmedida() {

		med=document.getElementById('medida').value;
		if(med!=0){
			document.getElementById('dmedida').className="row";
			if(tmed[med]>1){
				document.getElementById('emedida').placeholder=emed[med];
				document.getElementById('emedida').className="form-control";
			}else{
				document.getElementById('emedida').className="hide";
			}
			if(tmed[med]==2){
				// document.getElementById('emedida').className="";
				document.getElementById('med1').className="col-md-8";
				document.getElementById('med2').className="col-md-4";
				document.getElementById('emedida').value='menos de 50 personas.';
				cmed=1;
			}else{
				// document.getElementById('emedida').className="form-control";
				document.getElementById('med1').className="col-md-12";
				document.getElementById('med2').className="hide";
				document.getElementById('emedida').value='';
			}
		}else{
			document.getElementById('dmedida').className="hide";
		}
	}
	function addnmed () {
		if(cmed<5){
			cmed++;
			document.getElementById('emedida').value=nmeds[cmed];	
		}
	}
	function susnmed () {
		if(cmed>1){
			cmed--;
			document.getElementById('emedida').value=nmeds[cmed];	
		}
	}
	function aotro() {
		otra=document.getElementById('tamenaza').value;
		if(otra==1000){
			document.getElementById('dotraa').className='';
		}else{
			document.getElementById('dotraa').className='hide';
		}

	}
	function abrirVentana(url) {
        window.open(url, "nuevo", "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=800, height=640");
        // verif();      
    }

</script>
