<?php //$datos=$_POST; 
$dias[1]="lunes";
$dias[2]="martes";
$dias[3]="miercoles";
$dias[4]="jueves";
$dias[5]="viernes";
$dias[6]="sabado";
$dias[0]="domingo";

$meses[1]="enero";
$meses[2]="febrero";
$meses[3]="marzo";
$meses[4]="abril";
$meses[5]="mayo";
$meses[6]="junio";
$meses[7]="julio";
$meses[8]="agosto";
$meses[9]="septiembre";
$meses[10]="octubre";
$meses[11]="noviembre";
$meses[12]="diciembre";

$prod=array();
$prod[1]='Gobierno';
$prod[2]='Otras Fuentes Oficiales del Estado';
$prod[3]='Cuerpo Dip, Org. Int, ONGs';
$prod[4]='Empresa Privada';
$prod[5]='Sociedad Civil';

$vr=array();
$vr[1]='Muy Confiable';
$vr[2]='Confiable';
$vr[3]='Medianamente Confiable';
$vr[4]='Poco Confiable';
$vr[5]='Desconfiable';
$vr[6]='Nunca se trabajo con la fuente';

$rubs=array();
$rubs[1]="Agencias de 
Cooperación";
$rubs[2]="Bancos";
$rubs[3]="Empresas Privadas";
$rubs[4]="Otros";

$pert=array();
$pert[1]='Muy Pertinente';
$pert[2]='Pertinente';
$pert[3]='Medianamente Pertinente';
$pert[4]='Poco Pertinente';
$pert[5]='No Pertinente';
$pert[0]='Se desconoce la Pertinencia';

$cred=array();
$cred[1]='Muy Creible';
$cred[2]='Creible';
$cred[3]='Medianamente Creible';
$cred[4]='Poco Creible';
$cred[5]='No Creible';
$cred[6]='Nunca se recibio este tipo de información'; 

$riesgo=array(); 

$riesgo[1]='Muy Bajo';
$riesgo[2]='Bajo';
$riesgo[3]='Medio';
$riesgo[4]='Alto';
$riesgo[5]='Muy Alto';

$amb=array();
$amb[1]="Nacional";
$amb[2]='Regional';
$amb[3]='Departamental';
$amb[4]='Municipal';
$amb[5]='Vecinal/Comunal';
$amb[6]='Institucional';

$prob=array();
$prob[5]='Muy Alta (80% - 100%)';
$prob[4]='Alta (60% - 80%)';
$prob[3]='Media (40% - 60%)';
$prob[2]='Poca (20% - 40%)';
$prob[1]='Ninguna (0% - 20%)';

if(isset($sector)){
	$eje=array();
	$eje[0]="Sector Afectado/Parte Demandada";
	foreach ($sector as $value) {
		$eje[$value['id']]=$value['nombre'];
	}
}


$itipo='';
$con="";

?>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<br>
					<div class="row">
						<div class="col-md-10">
						    <h4>Ver Información de Seguridad</h4>
						    <p>Formulario de información de seguridad.</p>
					    </div>
					    <div class="col-md-2 right">
					    	<button class="btn btn-danger btn-sm" onclick="window.close();">X</button>
					    </div>
				    </div>
				<table class="table">
									<tr>
										<td width="30%">Titular:</td>
										<td width="70%"><?php echo $datos['nombre'] ?></td>
									</tr>
									<tr>
										<td>Amenaza: </td>
										<td><?php  echo $caso=$datos['amenaza'];
											if($sub!=array()) echo ': <br>';
												foreach ($sub as $value) {
													echo join('/',$value),'<br>';
												}if($datos['tipo']==4)
												echo '<br>Ambito: '.$amb[$datos['conflicto']];
											 ?> </td>
									</tr>
									<tr>
										<td>Codigo:</td>
										<td><?php echo $datos['codigo'] ?></td>
									</tr>
									<tr>
										<td>Estado:</td>
										<td><?php if($datos['estado']==0) echo 'Evento en Desarrollo';
													elseif ($datos['estado']==2) echo 'Noticia Relicionada';
													else echo 'Evento Anunciado' ?>
										</td>
									</tr>
									<tr>
										<td>Fecha del Registro</td>
										<td><?php echo date('d/m/Y', strtotime($datos['fecha_notificacion'])) ?></td>
									</tr>
									<tr>
										<td>Medio:</td>
										<td><?php if($datos['tmedio']!=0)
											echo $datos['tmedio'].'<br>'.$datos['medio'];
											else echo 'Información Extraoficial' ?>
										</td>
									</tr>
								</table>
								<h4>Fuente</h4>
								<table class="table">
									<tr>
										<td>Procedencia:</td>
										<td><?php echo $prod[$datos['procedencia']]; ?>
										</td>
									</tr>
									<tr>
										<td>Valoración:</td>
										<td><?php echo $vr[$datos['valoracion']] ?>
										</td>
									</tr>
								</table>
								<h4>Información</h4>
								<table class="table">
									<tr>
										<td>Credibilidad: <i class="fa fa-help"></i></td>
										<td><?php echo $cred[$datos['credibilidad']]  ?>
										</td>
									</tr>
									<tr>
										<td>Pertinencia: <i class="fa fa-help"></i></td>
										<td><?php $rubros=explode(';',$datos['pertinencia']);
											foreach ($rubros as $value) {
												$par=explode('-',$value);
												echo $rubs[$par[0]].': '.$pert[$par[1]].'<br>';
											}
										 ?>
										</td>
									</tr>
								</tABLE>
								<?php if($datos['tipo']==4){ ?>
								<h4>Actores</h4>
								<table class="table">
									<tr><th>Generadores del Conflicto / Parte Demandada</th></tr>
									<tr><td><?php foreach ($anta as $value) {
										echo $value['nombre'].', ';
									} ?>
									</td></tr>
									<tr><th>Sector Afectado / Parte Demandante</th></tr>
									<tr><td><?php foreach ($prota as $value) {
										echo $value['nombre'].', ';
									} ?>
									</td></tr>
								</table>
								<table class="table">
									<tr>
										<th>Tipo de Medida: </th>
										<td><?php if($datos['medida']=='') echo 'Ninguno'; else echo $datos['medida'].' ('.$datos['nmedida'].')'; ?>
										</td>
									</tr>
									<?php if($datos['medida']!=''){ ?>
									<tr>
										<th>Detalles: </th>
										<td><?php echo 'Inicio: '.sprintf("%'.02d", $datos['inicio']).':00, Fin: '.sprintf("%'.02d", $datos['fin']).':00'; ?>
										</td>
									</tr>
									<tr>
										<th>Ejecutado por: </th>
										<td><?php echo $eje[$datos['ejecutado']]; ?>
										</td>
									</tr>
									<?php } ?>
								</tABLE>
								<?php } ?>
								<h4>Noticia</h4>
								<table class="table">
									<tr>
										<td width="30%">Valoración de Riesgo: </td>
										<td width="70%"><?php echo $riesgo[$datos['val_riesgo']] ?></td>
									</tr>
									<tr>
										<td>Resumen de la Noticia: </td>
										<td align="justify"><?php echo $datos['resumen'] ?>
										</td>
									</tr>
									<tr>
										<td>Lugar de la Noticia: </td>
										<td><?php echo $datos['lugar'] ?></td>
									</tr>
									<tr>
										<td>Ubicación en el Mapa: </td>
										<td><?php echo $datos['coordenadas'] ?></td>
									</tr>
									<tr>
										<td>Fecha de la Noticia: </td>
										<td><?php echo date('d/m/Y', strtotime($datos['fecha_noticia'])) ?></td>
										</td>
									</tr>
									<tr>
										<td>Comentario: </td>
										<td><?php echo $datos['descripcion'] ?></td>
									</tr>
									<?php if($datos['enlace']!=''){ ?>
									<tr>
										<td>Enlace de Referencia: </td>
										<td><a href="<?php echo $datos['enlace'] ?>"><?php echo $datos['enlace'] ?></a></td>
									</tr>
									<?php } ?>
									<tr>
										<td>Imagen: </td>
										<td>
											<?php if($datos['imagen']!=''){ ?>
												<img class="img-thumbnail img-fluid img-responsive" style="max-width: 100%; and height: auto;" src="<?php echo base_url('img/noticias/'.$datos['imagen']) ?>">
											<?php }else{ ?>
												<img class="img-thumbnail img-fluid img-responsive" style="max-width: 100%; and height: auto;" src="<?php echo base_url('img/noticias/0.png') ?>">
											<?php } ?>
										</td>
									</tr>
									<tr>
								        <td width="30%">Parpadeo del evento en el mapa: </td>
							        	<td width="70%">
							        		<?php if($datos['imp']==1) echo 'Activado';else echo "Desactivado" ?>
								        </td>
							    	</tr>
								</table>

								<h4>Probabilidad e Impacto</h4>
								<table class="table">
									<tr>
										<th>Probabilidad de que el evento se repita:</th>
										<td><?php echo $prob[$datos['prob']] ?>
										</td>
									</tr>
									<?php foreach ($imp as $value) { 
										if($value['tipo']!=$itipo){ 
											$itipo=$value['tipo']; $con=""; ?>
										</td>
									</tr>
									<tr>
										<th>Impacto <?php echo $value['tipo'] ?>:</th>
										<td>
									<?php }echo $con.$value['cantidad'].' '.$value['nombre']; $con=", ";
									}  ?>
								</table>
			</div>
			<div class="col-md-1"></div>
			</div>
		</div>
	</div>
</body>
