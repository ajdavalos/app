<body>
	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<br>
				<h4>Formulario</h4>
				<hr>
				<p class="center">
					<input id="department" class="form-control" placeholder="departamento">
					<input id="zone" class="form-control" placeholder="zona">
					<input id="latitude" class="form-control" placeholder="latitude">
					<input id="longitude" class="form-control" placeholder="longitude">
					<input id="city" class="form-control" placeholder="ciudad">
					<input id="doornumber" class="form-control" placeholder="numero de casa/puerta">
					<input id="route" class="form-control" placeholder="calle o avenida">
				</p>
				<button class="btn btn-info" onclick="abrirVentana('<?php echo base_url('inicio/ubicacion') ?>')">Ver Mapa</button>
			</div>
			</div>
			<div class="col-md-2"></div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">

function abrirVentana(url) {
        window.open(url, "nuevo", "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=800, height=640");
        // verif();      
    }

</script>
			